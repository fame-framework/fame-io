<!-- SPDX-FileCopyrightText: 2023 German Aerospace Center <fame@dlr.de>

SPDX-License-Identifier: Apache-2.0 -->

# Contribute
Please read the Contributors License Agreement `cla.md`, sign it and send it to [`fame@dlr.de`](mailto:fame@dlr.de) before contributing.

You will also find templates for [bug reports](https://gitlab.com/fame-framework/fame-io/-/blob/main/.gitlab/issue_templates/bug_report.md), [feature requests](https://gitlab.com/fame-framework/fame-io/-/blob/main/.gitlab/issue_templates/feature_request.md), and [pull requests](https://gitlab.com/fame-framework/fame-io/-/blob/main/.gitlab/issue_templates/pull_request.md).

Please see our [conventions of contribution](https://gitlab.com/fame-framework/wiki/-/wikis/developers/contribute/Conventions) which are described in the Wiki.

## Code Environment
We strongly recommend that you create a fresh environment to develop and test FAME-IO. The dependencies are managed with the python package [Poetry](https://python-poetry.org/), which uses a `pyproject.toml` file to detect and install the right dependencies. To set everything up, switch to your local repository folder and run

```bash
    conda create -n env-fameio python=3.10 poetry
    conda activate env-fameio
    poetry install --with dev
```

It is recommended to use an up-to-date Python IDE to develop fameio code.

## Testing changes locally
Once some changes have been performed on the local git clone, use the following command to override your local installation with your modified copy in order to test the result:

```bash
    python3 -m build && pip3 install --force-reinstall --no-dependencies ./dist/*.whl
```

## Code style
Please follow the PEP-8 style guide for Python, but limit line length to 120.
Please follow Google's conventions for docstrings.
Use the code formatting library [`black`](https://pypi.org/project/black/) and run `black .` before committing.

## Pre-Commit hooks
FAME-Io uses several pre-commit hooks to ensure high code quality.
To use them, install `dev` packages and then initialise pre-commit in FAME-Io's base folder:

```
    pip install fameio[dev]
    pre-commit install -t pre-commit -t pre-push
```

## List of Contributors
The following people made significant contributions to FAME-Io (in calendric order of their contribution):
* [Marc Deissenroth-Uhrig](https://orcid.org/0000-0002-9103-418X)
* [Ulrich Frey](https://orcid.org/0000-0002-9803-1336) (@litotes18)
* [Benjamin Fuchs](https://orcid.org/0000-0002-7820-851X) (@dlrbf)
* [Christoph Schimeczek](https://orcid.org/0000-0002-9824-3371) (@dlr-cjs)
* [Felix Nitsch](https://orcid.org/0000-0002-9824-3371) (@dlr_fn)
* [Johannes Kochems](https://orcid.org/0000-0002-3461-3679) (@dlr_jk)
* Aurelien Regat-Barrel (@aurelienrb)
* [Florian Maurer](https://orcid.org/0000-0001-8345-3889) (@maurerle)
* [Leonard Willeke](https://orcid.org/0009-0004-4859-2452) (@LeonardWilleke)
