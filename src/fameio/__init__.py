# SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: CC0-1.0

FILE_HEADER_V1 = "famecoreprotobufstreamfilev001"  # noqa
FILE_HEADER_V2 = "fameprotobufstreamfilev002    "  # noqa
