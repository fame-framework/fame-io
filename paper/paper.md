---
title: 'FAME-Io: Configuration tools for complex agent-based simulations'
tags:
  - Python
  - agent-based
  - energy systems
  - data conversion
authors:
  - name: Felix Nitsch^[corresponding author]
    orcid: 0000-0002-9824-3371
    affiliation: 1
  - name: Christoph Schimeczek
    orcid: 0000-0002-0791-9365
    affiliation: 1
  - name: Ulrich Frey
    orcid: 0000-0002-9803-1336
    affiliation: 1
  - name: Benjamin Fuchs
    orcid: 0000-0002-7820-851X
    affiliation: 1

affiliations:
 - name: German Aerospace Center (DLR), Institute of Networked Energy Systems, Curiestr. 4, 70563 Stuttgart, Germany
   index: 1

date: 09 January 2023
bibliography: paper.bib
---

# Summary
We present FAME‑Io, a Python package designed to help users and creators of agent-based simulation models (ABM) better manage the preparation and processing of their input and output datasets. 
The package was built with the needs of researchers in mind.
FAME‑Io was specifically developed to interface with the open framework FAME^[https://gitlab.com/fame-framework/] and is published under the open Apache-2.0 licence.
The software offers various logging capabilities, shell‑integrated help and documentation, as well as extensive pre‑run integrity checks and helpful warning messages. 
It also allows individual data components to be easily extracted and used in secondary workflows.
The code itself is operating system independent and follows best practices in software development.
Test coverage, at the time of writing, is 92% and the project uses continuous integration and offers frequent releases.

FAME-Io is designed as one of two main components of FAME, each addressing a particular aspect and user group within ABM development, see \autoref{fig:components}:

* FAME-Io is a Python package^[https://pypi.org/project/fameio/] for ABM users. It supports configuring complex simulations and managing of files associated with FAME.
* FAME-Core [@FAME_CORE] is a Java library for ABM modellers. It supports developing and executing ABM simulations.

These two modules interact with additional supporting components, like FAME-Mpi^[https://gitlab.com/fame-framework/mpi], 
FAME-Protobuf^[https://gitlab.com/fame-framework/fame-protobuf], FAME-Gui^[https://gitlab.com/fame-framework/fame-gui] (in beta release) and FAME-Prepare^[https://gitlab.com/fame-framework/fame-prepare] (under development). 
The demonstration project FAME-Demo^[https://gitlab.com/fame-framework/fame-demo] provides a ready-made example for modellers to investigate and experiment with. 

![Components of FAME and their interactions.\label{fig:components}](Components.png)

## Configuration of FAME model inputs
Defining agents and their interactions is the first central task when creating any ABM simulation.
IN FAME, such agent interactions need not be hard-coded, but can be defined when configuring the model. 
FAME-Io enables researchers to quickly create and handle configurations of simulations, even complex ones with thousands of agents and numerous interactions.
To this end, FAME-Io allows splitting configurations into multiple files, each dedicated to a specific aspect of a configuration. 
An overarching configuration comprises one or more structured YAML files.
Large data structures, like time series, can be imported from external (CSV) files.
FAME-Io supports easy definition of multiple similar agent interactions, i.e., "Many-to-One" , "One-to-Many" and "Many-to-Many" messages, significantly reducing repetitive parameterization tasks.

Once the simulation configuration files are created, they are validated by FAME-Io against a schema derived from the associated FAME model. 
This validation is comprehensive and checks the presence of each agent's mandatory parameters.
It also tests the data type of each parameter and possible value restrictions.
Furthermore, FAME-Io evaluates the plausibility of agent interactions, i.e., the correct type of data sent for each agent interaction.
If errors are detected, FAME-Io issues meaningful messages that help researchers to fix configuration problems even before the actual simulation runs.
In case no configuration errors are identified, FAME-Io creates a single binary protobuf file that serves as input for the associated ABM (executed with FAME-Core).

## Conversion of FAME model outputs
Once a model is executed with FAME-Core, its entire output is stored in a single binary protobuf file. 
FAME-Io converts this output into human-readable files with a standardized output structure in CSV format.
Researchers can customize this configuration process to suit their needs. 
FAME-Io allows limiting the conversion to subsets of agents, it offers different output formats, and the results of similar agents can be split or grouped.
These options facilitate later individual post-processing of the results.
FAME-Io can also handle very large output files, e.g., from simulations with thousands of agents.
For computers with limited resources, it offers a mode with a low-memory profile.

# Statement of need
The energy system transition towards a sustainable future is one of the most urgent concerns of our time.
Hence, the field of energy systems analysis deals with possible paths to a more sustainable future.
However, the increasing complexity of energy markets combined with different policies and heterogeneous actors poses ever new challenges to this field [@pfenninger2014energy; @pye2021modelling]. 
ABM is perhaps the most suitable approach to tackle these challenges [@klein2019], because

* the actors’ perspective and their specific characteristics are taken into account [@frey2020self; @kraan2018investment], and
* models remain computationally feasible even in large-scale simulations [@hansen2019agent].

FAME is a new software suite that helps researchers develop and use ABM simulations.
FAME-models are highly configurable and allow the modelling workflow to be changed without affecting the model itself.
This high flexibility of models, however, leads to the need for a sophisticated model configuration tool - FAME-Io addresses this need by providing powerful tools for complex ABM.
It provides automatic translation between Java-coded agent specifications and highly organised configuration files.
FAME-Io grants a convenient way to interact with FAME-based models, i.e. to input data and access their results.
It also helps model users handling complex simulation configurations with hundreds of repeated interactions between agents.

## State of the field
A scholarly search returns mostly review articles, e.g., by @RINGKJOB2018440.
However, configuration support or input-output management is not an issue in any of them.
Hence, we compare the two most popular ABM frameworks, NetLogo [@wilensky1999], which is widely used in teaching, and Repast [@Repast], which was recently ported to Python.
Unlike FAME-Io, both frameworks lack a dedicated input-output management and a configuration and validation tool.
Help with parameterizing a NetLogo model is only provided by BehaviorSpace^[https://ccl.northwestern.edu/netlogo/docs/behaviorspace.html].
Given this lack of tools, we conclude that the need for a dedicated software is there and FAME-Io can fill this need.

## Performance
FAME-Io offers high performance.
It reads, validates, and converts configurations with approximately 25 agents, 150 contracts and one megabyte of time series data in less than five seconds on a desktop PC.
However, for very large configurations, FAME-Io can run on multiple computing cores for further speed increase. 
The YAML and CSV file types used by FAME-Io are easily readable by both humans and machines.
Thus, the configuration, execution, and result evaluation of FAME-based models can be easily integrated into automated workflows via FAME-Io. 
This paves the way for the use of FAME models as part of computationally intensive approaches, such as extensive parameter sweeps or genetic algorithms.

# Applications
FAME was designed for energy systems modelling, but is not limited to this field.
Thus, it can also be used in other domains for ABM simulations with similarly structured problems.
FAME-Io is an integral part of FAME and currently the only tool available for configuring FAME simulations and accessing their results.

A large and powerful FAME-based model is AMIRIS [@AMIRIS]^[https://dlr-ve.gitlab.io/esy/amiris/home/], an agent-based market model for the investigation of renewable and integrated energy systems.
AMIRIS is also open-source and is being used in current projects in the field of energy systems analysis.
Specifically, FAME-Io has been used to integrate AMIRIS into multi-model workflows with automatic model configuration and result evaluation in the following projects:
TradeRES^[https://traderes.eu/], ERAFlex II^[https://www.enargus.de/detail/?id=2001065], UNSEEN^[https://www.dlr.de/ve/en/desktopdefault.aspx/tabid-15957/25887_read-66481/], and VERMEER^[https://www.dlr.de/ve/en/desktopdefault.aspx/tabid-15955/25885_read-66475/].
Hence, it has already been applied for various scientific publications [@frey2020self; @Seyedfarzad9221924; @nitsch_felix_2021_5726738].

# Funding
This project was funded by the German Aerospace Center (DLR).

# Acknowledgments
The authors would like to thank Aurélien Regat-Barrel, who worked on the interface for FAME-Gui, for his advice and comments.
Furthermore, the authors thank Manuel Bostanci for implementing parallelisation of FAME-Io configuration tasks.

# References
