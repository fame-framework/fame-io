# SPDX-FileCopyrightText: 2025 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
import logging
import os
from pathlib import Path

import pandas as pd
import pytest
from google.protobuf.internal.wire_format import INT64_MIN, INT64_MAX
from mockito import when, ANY  # noqa

from fameio.series import TimeSeriesManager, TimeSeriesError
from tests.log_utils import fame_log
from tests.output.input_mocks import mock_series


class TestTimeSeriesManager:
    _DUMMY_FRAME_VALID = pd.DataFrame(data={0: [0], 1: [1]})
    _DUMMY_FRAME_FLOAT = pd.DataFrame(data={0: [1.0], 1: [1.0]})

    @pytest.fixture
    def manager(self):
        return TimeSeriesManager()

    @pytest.mark.parametrize("values", [[0, 0], [2, 2], [2.0, 2], [-1, -1], [2000.0, 2000], [2, 2, 2, 2, 2]])
    def test_register_and_validate_same_numeric_identifiers__yield_same_id(self, values, manager):
        manager.register_and_validate(values[0])
        ids = [manager.get_series_id_by_identifier(i) for i in values]
        assert len(set(ids)) == 1

    @pytest.mark.parametrize("values", [["a", "a"], ["my/file/path.csv", "my/file/path.csv"]])
    def test_register_and_validate_same_string_identifiers__yield_same_id(self, manager, values):
        with when(pd).read_csv(ANY, sep=";", header=None, comment="#").thenReturn(self._DUMMY_FRAME_VALID), when(
            os.path
        ).exists(ANY).thenReturn(True):
            for value in values:
                manager.register_and_validate(value)
        ids = [manager.get_series_id_by_identifier(i) for i in values]
        assert len(set(ids)) == 1

    @pytest.mark.parametrize("values", [[2, 3], [-1, 1], [0, 0.5, 1, 2, 4]])
    def test_register_and_validate__different_numeric_identifiers__yield_different_id(self, values, manager):
        for value in values:
            manager.register_and_validate(value)
        id_set = set([manager.get_series_id_by_identifier(value) for value in values])
        assert len(id_set) == len(values), "Not all created IDs are different"

    def test_register_and_validate__missing_file_raises(self, manager, fame_log):
        with when(os.path).exists(ANY).thenReturn(False), pytest.raises(TimeSeriesError) as e_info:
            manager.register_and_validate("missing file")
        fame_log.assert_logged_exception(TimeSeriesManager._ERR_FILE_NOT_FOUND, e_info)

    @pytest.mark.parametrize("value", ["1", "42.", "42.0", "4.2E1"])
    def test_register_and_validate__missing_file_numeric_string__raises_with_hint(self, fame_log, manager, value):
        with when(os.path).exists(ANY).thenReturn(False), pytest.raises(TimeSeriesError) as e_info:
            manager.register_and_validate(value)
        fame_log.assert_logged_exception(TimeSeriesManager._ERR_FILE_NOT_FOUND, e_info)
        fame_log.assert_logged_exception(TimeSeriesManager._ERR_NUMERIC_STRING, e_info)

    @pytest.mark.parametrize(
        "value, msg",
        [
            ({0: ["NotATimeStamp"], 1: [1]}, TimeSeriesManager._ERR_CORRUPT_TIME_SERIES_KEY),
            ({0: [1], 1: ["NotNumeric"]}, TimeSeriesManager._ERR_CORRUPT_TIME_SERIES_VALUE),
            ({0: [1], 1: [float("NaN")]}, TimeSeriesManager._ERR_NAN_VALUE),
            ({0: [1, 2, 3], 1: [1.0, None, 2.0]}, TimeSeriesManager._ERR_NAN_VALUE),
        ],
    )
    def test_register_and_validate__invalid_series__raises(self, fame_log, manager, value, msg: str):
        fame_log.set_level(logging.ERROR)
        with when(pd).read_csv(ANY, sep=";", header=None, comment="#").thenReturn(pd.DataFrame(data=value)), when(
            os.path
        ).exists(ANY).thenReturn(True), pytest.raises(TimeSeriesError) as e_info:
            manager.register_and_validate("file")
        fame_log.assert_logged_exception(msg, e_info)

    @pytest.mark.parametrize(
        "value",
        [
            {0: [1, 2, 3], 1: [1.0, 2.0, 3.0], 2: [1.0, 2.0, 3.0]},
            {0: [1, 2, 3], 1: [1.0, 2.0, 3.0], 2: [None, None, None], 3: [1.0, 2.0, 3.0]},
        ],
    )
    def test__register_and_validate__additional_columns_in_series__logs_warning(self, fame_log, manager, value):
        fame_log.set_level(logging.WARNING)
        with when(pd).read_csv(ANY, sep=";", header=None, comment="#").thenReturn(pd.DataFrame(data=value)), when(
            os.path
        ).exists(ANY).thenReturn(True):
            manager.register_and_validate("file")
        fame_log.assert_logged(TimeSeriesManager._WARN_DATA_IGNORED)

    def test__register_and_validate__no_additional_columns__no_warning(self, fame_log, manager):
        fame_log.set_level(logging.WARNING)
        df = pd.DataFrame(data={0: [1, 2, 3], 1: [1.0, 2.0, 3.0]})
        with when(pd).read_csv(ANY, sep=";", header=None, comment="#").thenReturn(df), when(os.path).exists(
            ANY
        ).thenReturn(True):
            manager.register_and_validate("file")
        fame_log.assert_no_logs()

    def test_get_unique_timeseries_id__nan_identifier__raises(self, fame_log, manager):
        fame_log.set_level(logging.ERROR)
        with pytest.raises(TimeSeriesError) as e_info:
            manager.register_and_validate(float("NaN"))
        fame_log.assert_logged_exception(TimeSeriesManager._ERR_NAN_VALUE, e_info)

    def test_get_series_id_by_identifier(self, fame_log, manager):
        fame_log.set_level(logging.ERROR)
        with pytest.raises(TimeSeriesError) as e_info:
            manager.get_series_id_by_identifier("not yet registered")
        fame_log.assert_logged_exception(TimeSeriesManager._ERR_UNREGISTERED_SERIES, e_info)

    def test_get_all_series__all_ids_contained(self, manager):
        manager.register_and_validate(3.0)
        manager.register_and_validate(99)
        ids = [i for i, _, _ in manager.get_all_series()]
        assert len(set(ids)) == 2

    def test_get_all_series__names_match_file_or_creation_pattern(self, manager):
        manager.register_and_validate(3)
        with when(pd).read_csv(ANY, sep=";", header=None, comment="#").thenReturn(self._DUMMY_FRAME_VALID), when(
            os.path
        ).exists(ANY).thenReturn(True):
            manager.register_and_validate("myFile.csv")

        names = [name for _, name, _ in manager.get_all_series()]
        assert TimeSeriesManager._CONSTANT_IDENTIFIER.format(3) in names
        assert "myFile.csv" in names

    def test_get_all_series__series_created_from_constant__match_format(self, manager):
        manager.register_and_validate(3.3)
        _, _, series = manager.get_all_series()[0]
        assert list(series[0]) == [INT64_MIN, INT64_MAX]
        assert list(series[1]) == [3.3, 3.3]

    def test_get_all_series__series_from_file__match_format(self, manager):
        with when(pd).read_csv(ANY, sep=";", header=None, comment="#").thenReturn(self._DUMMY_FRAME_VALID), when(
            os.path
        ).exists(ANY).thenReturn(True):
            manager.register_and_validate("myFile.csv")
        _, _, series = manager.get_all_series()[0]
        assert series[0][0] == 0
        assert series[1][0] == 1

    def test__get_all_series__float_data__time_column_converted_to_int(self, manager):
        with when(pd).read_csv(ANY, sep=";", header=None, comment="#").thenReturn(self._DUMMY_FRAME_FLOAT), when(
            os.path
        ).exists(ANY).thenReturn(True):
            manager.register_and_validate("myFile.csv")
        _, _, series = manager.get_all_series()[0]
        assert series[0][0] == 1
        assert series[1][0] == 1.0

    def test__get_reconstructed__series_by_id__negative_id__raises(self, fame_log, manager):
        with pytest.raises(TimeSeriesError) as e_info:
            manager.get_reconstructed_series_by_id(-1)
        fame_log.assert_logged_exception(TimeSeriesManager._ERR_UNREGISTERED_SERIES, e_info)

    def test__get_reconstructed__series_by_id__too_large_id__raises(self, fame_log, manager):
        with pytest.raises(TimeSeriesError) as e_info:
            manager.get_reconstructed_series_by_id(1)
        fame_log.assert_logged_exception(TimeSeriesManager._ERR_UNREGISTERED_SERIES, e_info)

    def test__reconstruct_time_series__get_reconstructed_series_by_id__returns_path_based_on_name(self, manager):
        name = "MySeries.csv"
        manager.reconstruct_time_series([mock_series(series_id=0, series_name=name, data=[])])
        result_name = manager.get_reconstructed_series_by_id(0)
        assert result_name == str(Path(TimeSeriesManager._TIMESERIES_RECONSTRUCTION_PATH, name))

    def test__reconstruct_time_series__get_reconstructed_series_by_id__adds_csv_ending(self, manager):
        name = "MySeries"
        manager.reconstruct_time_series([mock_series(series_id=0, series_name=name, data=[])])
        result_name = manager.get_reconstructed_series_by_id(0)
        assert result_name == str(Path(TimeSeriesManager._TIMESERIES_RECONSTRUCTION_PATH, name + ".csv"))

    def test__reconstruct_time_series__get_reconstructed_series_by_id__single_entry__returns_value(self, manager):
        value = 5.0
        manager.reconstruct_time_series([mock_series(series_id=0, series_name="n/a", data=[(0, value)])])
        result_name = manager.get_reconstructed_series_by_id(0)
        assert result_name == value

    def test__reconstruct_time_series__get_constructed_series_by_id__two_identical_values__returns_value(self, manager):
        value = 5
        manager.reconstruct_time_series([mock_series(series_id=0, series_name="n/a", data=[(0, value), (1, value)])])
        result_name = manager.get_reconstructed_series_by_id(0)
        assert result_name == value

    def test__reconstruct_time_series__get_constructed_series_by_id__two_different_values__returns_path(self, manager):
        name = "myName"
        manager.reconstruct_time_series([mock_series(series_id=0, series_name=name, data=[(0, 1), (1, 2)])])
        result_name = manager.get_reconstructed_series_by_id(0)
        assert result_name == str(Path(TimeSeriesManager._TIMESERIES_RECONSTRUCTION_PATH, name + ".csv"))
