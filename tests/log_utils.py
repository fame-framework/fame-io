# SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
import pytest
from _pytest._code import ExceptionInfo  # noqa

from fameio import logs


class LogTester:
    """Returned by log testing fixture to control fameio logger, its level and content"""

    logs.fameio_logger(logs.DEFAULT_LOG_LEVEL.name)

    def __init__(self, log_capture_fixture: pytest.LogCaptureFixture) -> None:
        self.log_capture = log_capture_fixture
        logs.log().propagate = True

    @staticmethod
    def set_level(level: int) -> None:
        """
        Set level of fameio logger

        Args:
            level: target level as defined by logging
        """
        logs.log().setLevel(level)

    def assert_logged(self, message: str) -> None:
        """
        Asserts that given message was logged at the current log level

        Args:
            message: to be searched for in the logs
        """
        self.assert_expected_message_in_actual(message, self.log_capture.text)

    @staticmethod
    def assert_expected_message_in_actual(expected: str, actual: str):
        """
        Return True if the expected message is contained in the actual string

        Args:
            expected: message, may contain `{}` or `%s` formatting placeholders
            actual: message to be tested to contain `expected`
        """
        removed_placeholders = expected.split(sep="{}") if "{}" in expected else expected.split(sep="%s")
        for message_part in removed_placeholders:
            assert message_part in str(actual)

    def assert_logged_exception(self, message: str, e_info: ExceptionInfo):
        """
        Asserts that given message is a. in the logs, and b. contained in the given exception
        Args:
            message: to be searched for in the logs and compared with the exception text
            e_info: exception to contain the given message
        """
        self.assert_logged(message)
        self.assert_expected_message_in_actual(message, str(e_info))

    def assert_no_logs(self):
        """Asserts that nothing was logged"""
        assert not self.log_capture.messages

    @staticmethod
    def _finalise():
        logs.log().propagate = False
        logs.log().setLevel(logs.DEFAULT_LOG_LEVEL.value)


@pytest.fixture
def fame_log(caplog) -> LogTester:
    """Enables testing of fameio logs with caplog"""
    log_tester = LogTester(caplog)
    yield log_tester
    # noinspection PyProtectedMember
    log_tester._finalise()
