# SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
import argparse
from argparse import ArgumentTypeError
from pathlib import Path

import pytest

from fameio.cli.options import ResolveOptions, TimeOptions, Options

# noinspection PyProtectedMember
from fameio.cli.parser import (
    add_file_argument,
    add_select_agents_argument,
    add_logfile_argument,
    add_output_argument,
    add_log_level_argument,
    add_encoding_argument,
    add_single_export_argument,
    add_memory_saving_argument,
    add_inputs_recovery_argument,
    add_resolve_complex_argument,
    add_time_argument,
    add_merge_time_argument,
    _ERR_INVALID_MERGING_DEFAULT,
    update_default_config,
    _OPTION_ARGUMENT_NAME,
    map_namespace_to_options_dict,
)
from tests.utils import assert_exception_contains


class TestParser:
    @pytest.fixture
    def parser(self):
        return argparse.ArgumentParser()

    def test__add_file_argument__missing_argument_no_default__raises(self, parser):
        add_file_argument(parser, None, "")
        with pytest.raises(SystemExit):
            parser.parse_args([])

    def test__add_file_argument__missing_file_no_default__raises(self, parser):
        add_file_argument(parser, None, "")
        with pytest.raises(SystemExit):
            parser.parse_args(["-f"])

    @pytest.mark.parametrize("value", ["a", "file.x", "path/to/file", "./rel/path.x", "/abs/path/file.b"])
    def test__add_file_argument__argument_and_file_specified__returns_path(self, parser, value: str):
        add_file_argument(parser, None, "")
        result = parser.parse_args(["-f", value])
        assert result.file == Path(value)

    def test__add_file_argument__missing_argument_with_default__returns_default(self, parser):
        default_path = Path("my/default/file")
        add_file_argument(parser, default_path, "")
        result = parser.parse_args([])
        assert result.file == default_path

    def test__add_file_argument__argument_specified_with_default__returns_argument(self, parser):
        add_file_argument(parser, Path(), "")
        my_file_path = "my/Path/to/file"
        result = parser.parse_args(["-f", my_file_path])
        assert result.file == Path(my_file_path)

    def test__add_select_agents_argument__missing_argument__yields_default(self, parser):
        defaults = ["a", "b", "c"]
        add_select_agents_argument(parser, defaults)
        result = parser.parse_args([])
        assert result.agents == defaults

    def test__add_select_agents_argument__value_missing__yields_empty_list(self, parser):
        defaults = ["a", "b", "c"]
        add_select_agents_argument(parser, defaults)
        result = parser.parse_args(["-a"])
        assert result.agents == []

    @pytest.mark.parametrize("values", [["a"], ["x", "b"], ["some", "names", "of", "agents"]])
    def test__add_select_agents_argument__values_added_to_list(self, parser, values: list[str]):
        defaults = ["My", "Default", "AgentNames"]
        add_select_agents_argument(parser, defaults)
        args = ["-a", *values]
        result = parser.parse_args(args)
        assert len(result.agents) == len(values)
        assert set(result.agents).issubset(values)

    def test__add_logfile_argument__missing__yields_default(self, parser):
        default_path = Path("my/default/file")
        add_logfile_argument(parser, default_path)
        result = parser.parse_args([])
        assert result.logfile == default_path

    def test__add_logfile_argument__value_missing__raises(self, parser):
        add_logfile_argument(parser, Path())
        with pytest.raises(SystemExit):
            parser.parse_args(["-lf"])

    @pytest.mark.parametrize("value", ["a", "file.x", "path/to/file", "./rel/path.x", "/abs/path/file.b"])
    def test__add_logfile_argument__creates_path_from_any_value(self, parser, value):
        add_logfile_argument(parser, Path())
        result = parser.parse_args(["-lf", value])
        assert result.logfile == Path(value)

    @pytest.mark.parametrize("default", ["some", "/path/to/a", "./default/output.file"])
    def test_add_output_argument_missing_yields_default(self, parser, default):
        add_output_argument(parser, default, "")
        result = parser.parse_args([])
        assert result.output == Path(default)

    def test_add_output_argument_missing_value_raises(self, parser):
        add_output_argument(parser, "default", "")
        with pytest.raises(SystemExit):
            parser.parse_args(["-o"])

    @pytest.mark.parametrize("value", ["a", "file.x", "path/to/file", "./rel/path.x", "/abs/path/file.b"])
    def test_add_output_argument_value_overrides_default(self, parser, value):
        add_output_argument(parser, "default", "")
        result = parser.parse_args(["-o", value])
        assert result.output == Path(value)

    @pytest.mark.parametrize("default", ["ERROR", "DEBUG", "INFO"])
    def test_add_log_level_argument_missing_argument_yields_default(self, parser, default):
        add_log_level_argument(parser, default)
        result = parser.parse_args([])
        assert result.log == default

    def test_add_log_level_argument_missing_value_raises(self, parser):
        add_log_level_argument(parser, "ERROR")
        with pytest.raises(SystemExit):
            parser.parse_args(["-l"])

    def test_add_log_level_argument_unknown_level_raises(self, parser):
        add_log_level_argument(parser, "ERROR")
        with pytest.raises(SystemExit):
            parser.parse_args(["-l", "'not-an-error-level'"])

    @pytest.mark.parametrize("value", ["error", "ERROR", "eRRoR", "Critical", "inFo", "dEbUg"])
    def test_add_log_level_argument_overrides_default_with_valid_value(self, parser, value):
        add_log_level_argument(parser, "warning")
        result = parser.parse_args(["-l", value])
        assert result.log == value.upper()

    def test__add_encoding_argument__with_default_none__returns_none(self, parser):
        add_encoding_argument(parser, default_value=None, help_text="")
        result = parser.parse_args([])
        assert result.encoding is None

    def test__add_encoding_argument__argument__overrides_default(self, parser):
        add_encoding_argument(parser, default_value=None, help_text="")
        result = parser.parse_args(["-enc", "utf8"])
        assert result.encoding == "utf8"

    @pytest.mark.parametrize("default", [True, False])
    def test_add_single_export_argument_missing_yields_default(self, parser, default):
        add_single_export_argument(parser, default)
        result = parser.parse_args([])
        assert result.single_export == default

    def test_add_single_export_argument_present_yields_true(self, parser):
        add_single_export_argument(parser, False)
        result = parser.parse_args(["-se"])
        assert result.single_export

    @pytest.mark.parametrize("default", [True, False])
    def test_add_memory_saving_argument_missing_yields_default(self, parser, default):
        add_memory_saving_argument(parser, default)
        result = parser.parse_args([])
        assert result.memory_saving == default

    def test_add_memory_saving_argument_present_yields_true(self, parser):
        add_memory_saving_argument(parser, False)
        result = parser.parse_args(["-m"])
        assert result.memory_saving

    @pytest.mark.parametrize("default", [True, False])
    def test__add_recover_inputs_argument__arg_missing__yields_default(self, parser, default):
        add_inputs_recovery_argument(parser, default)
        result = parser.parse_args([])
        assert result.input_recovery == default

    @pytest.mark.parametrize(
        "param", [{"value": True, "cli": "--input-recovery"}, {"value": False, "cli": "--no-input-recovery"}]
    )
    def test__add_recover_inputs_argument__arg_present__yields_value(self, parser, param):
        add_inputs_recovery_argument(parser, not param["value"])
        result = parser.parse_args([param["cli"]])
        assert result.input_recovery == param["value"]

    @pytest.mark.parametrize("default", list(ResolveOptions))
    def test__add_resolve_complex_argument__missing__yields_default(self, parser, default: ResolveOptions):
        add_resolve_complex_argument(parser, default)
        result = parser.parse_args([])
        assert result.complex_column == default

    @pytest.mark.parametrize("default", [e.name for e in ResolveOptions])
    def test__add_resolve_complex_argument__string_default__works(self, parser, default: str):
        add_resolve_complex_argument(parser, default)
        result = parser.parse_args([])
        assert result.complex_column == ResolveOptions[default]

    def test__add_resolve_complex_argument__missing_value__raises(self, parser):
        add_resolve_complex_argument(parser, ResolveOptions.IGNORE)
        with pytest.raises(SystemExit):
            parser.parse_args(["-cc"])

    def test__add_resolve_complex_argument__invalid_value__raises(self, parser):
        add_resolve_complex_argument(parser, ResolveOptions.IGNORE)
        with pytest.raises(SystemExit):
            parser.parse_args(["-cc", "not_a_valid_value"])

    @pytest.mark.parametrize("value", list(ResolveOptions))
    def test__add_resolve_complex_argument__valid_values__override_default(self, parser, value: ResolveOptions):
        add_resolve_complex_argument(parser, ResolveOptions.IGNORE)
        result = parser.parse_args(["-cc", str(value)])
        assert result.complex_column == value

    @pytest.mark.parametrize("default", list(TimeOptions))
    def test__add_time_argument__missing__yields_default(self, parser, default: TimeOptions):
        add_time_argument(parser, default)
        result = parser.parse_args([])
        assert result.time == default

    @pytest.mark.parametrize("default", [e.name for e in TimeOptions])
    def test__add_time_argument__string_default__works(self, parser, default: str):
        add_time_argument(parser, default)
        result = parser.parse_args([])
        assert result.time == TimeOptions[default]

    def test__add_time_argument__missing_value__raises(self, parser):
        add_time_argument(parser, TimeOptions.INT)
        with pytest.raises(SystemExit):
            parser.parse_args(["-t"])

    def test__add_time_argument__invalid_value__raises(self, parser):
        add_time_argument(parser, TimeOptions.INT)
        with pytest.raises(SystemExit):
            parser.parse_args(["-t", "not_a_valid_value"])

    @pytest.mark.parametrize("value", [e.name for e in TimeOptions])
    def test__add_time_argument__valid_value__overrides_default(self, parser, value: str):
        add_time_argument(parser, TimeOptions.INT)
        result = parser.parse_args(["-t", value])
        assert result.time == TimeOptions[value]

    @pytest.mark.parametrize("default", [None, []])
    def test__add_merge_time_argument__missing_and_no_default__returns_empty_list(self, parser, default):
        add_merge_time_argument(parser, default)
        result = parser.parse_args([])
        assert result.merge_times == []

    @pytest.mark.parametrize("default", [{}, "NotAList", [1], [1, 2], [1, 2, "C"]])
    def test__add_merge_time_argument__invalid_default__raises(self, parser, default):
        with pytest.raises(ArgumentTypeError) as e_info:
            add_merge_time_argument(parser, default)
        assert_exception_contains(_ERR_INVALID_MERGING_DEFAULT, e_info)

    def test__add_merge_time_argument__no_argument__yields_default(self, parser):
        default = [0, 1, 2]
        add_merge_time_argument(parser, default)
        result = parser.parse_args([])
        assert result.merge_times == default

    def test__add_merge_time_argument__missing_value__raises(self, parser):
        add_merge_time_argument(parser, None)
        with pytest.raises(SystemExit):
            parser.parse_args(["-mt"])

    @pytest.mark.parametrize("values", [["0"], ["0", "1"]])
    def test__add_merge_time_argument__not_enough_values__raises(self, parser, values):
        add_merge_time_argument(parser, None)
        with pytest.raises(SystemExit):
            parser.parse_args(["-mt", *values])

    @pytest.mark.parametrize("values", [["A", "1", "2"], ["1", "2", "42.2"]])
    def test__add_merge_time_argument__illegal_values__raises(self, parser, values):
        add_merge_time_argument(parser, None)
        with pytest.raises(SystemExit):
            parser.parse_args(["-mt", *values])

    @pytest.mark.parametrize("values", [["0", "1", "2", "3"], ["0", "1", "2", "3", "4", "5"]])
    def test__add_merge_time_argument__too_many_values__raises(self, parser, values):
        add_merge_time_argument(parser, None)
        with pytest.raises(SystemExit):
            parser.parse_args(["-mt", *values])

    @pytest.mark.parametrize("defaults", [[0], [0, 1]])
    def test__add_merge_time_argument__not_enough_defaults__raises(self, parser, defaults):
        with pytest.raises(ArgumentTypeError) as e_info:
            add_merge_time_argument(parser, defaults)
        assert_exception_contains(_ERR_INVALID_MERGING_DEFAULT, e_info)

    @pytest.mark.parametrize("defaults", [[0, 1, 2, 3], [0, 1, 2, 3, 4, 5]])
    def test__add_merge_time_argument__too_many_defaults__raises(self, parser, defaults):
        with pytest.raises(ArgumentTypeError) as e_info:
            add_merge_time_argument(parser, defaults)
        assert_exception_contains(_ERR_INVALID_MERGING_DEFAULT, e_info)

    @pytest.mark.parametrize("defaults", [["A", 1, 2], [1, 2, 42.2]])
    def test__add_merge_time_argument__illegal_defaults__raises(self, parser, defaults):
        with pytest.raises(ArgumentTypeError) as e_info:
            add_merge_time_argument(parser, defaults)
        assert_exception_contains(_ERR_INVALID_MERGING_DEFAULT, e_info)

    def test__add_merge_time_argument__argument__overrides_default(self, parser):
        default = [0, 1, 2]
        add_merge_time_argument(parser, default)
        result = parser.parse_args(["-mt", "7", "8", "9"])
        assert result.merge_times == [7, 8, 9]


class TestHelpers:
    _default = {"a": 1, "b": 2, "c": 3}

    def test_update_default_config_update_a(self):
        config = {"a": 42}
        expected = {"a": 42, "b": 2, "c": 3}
        assert update_default_config(config, self._default) == expected

    def test_update_default_config_update_abc(self):
        config = {"a": 42, "b": 42, "c": 42}
        expected = {"a": 42, "b": 42, "c": 42}
        assert update_default_config(config, self._default) == expected

    def test_update_default_config_update_abcd(self):
        config = {"d": 42}
        expected = {"a": 1, "b": 2, "c": 3, "d": 42}
        assert update_default_config(config, self._default) == expected

    def test_update_default_config_none(self):
        config = None
        expected = {"a": 1, "b": 2, "c": 3}
        assert update_default_config(config, self._default) == expected

    def test__map_namespace_to_options_dict__plain_options__mapped(self):
        namespace = argparse.Namespace()
        entries = {name: option for name, option in _OPTION_ARGUMENT_NAME.items() if isinstance(option, Options)}
        for name, option in entries.items():
            namespace.__setattr__(name, str(option))

        result = map_namespace_to_options_dict(namespace)
        for option in entries.values():
            assert option in result
            assert result[option] == str(option)
