# SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
import argparse

import pytest
from mockito import mock, when, verify, args, kwargs

from fameio.cli.make_config import (
    _get_default,
    CLI_DEFAULTS,
    _prepare_parser,
)
from fameio.cli.options import Options
from tests.mock_utils import un_stub


def test__get_default__default_exists__returns_default_value():
    result = _get_default({Options.LOG_LEVEL: "abc"}, Options.LOG_LEVEL)
    assert result == "abc"


def test__get_default__default_missing__returns_base_value():
    result = _get_default({}, Options.LOG_LEVEL)
    assert result == CLI_DEFAULTS[Options.LOG_LEVEL]


def test__prepare_parser__defaults__used(un_stub):
    parser_mock = mock(argparse.ArgumentParser)
    when(argparse).ArgumentParser().thenReturn(parser_mock)
    when(parser_mock).add_argument(*args, **kwargs)
    defaults = {
        Options.FILE: "this is a file",
        Options.LOG_LEVEL: "myLogLevel",
        Options.LOG_FILE: "myLogFile",
        Options.OUTPUT: "OutputPath",
    }
    _prepare_parser(defaults)
    for value in defaults.values():
        verify(parser_mock, times=1).add_argument(*args, default=value, **kwargs)


@pytest.mark.parametrize("defaults", [{}, None])
def test__prepare_parser__no_defaults__cli_defaults_used(un_stub, defaults):
    parser_mock = mock(argparse.ArgumentParser)
    when(argparse).ArgumentParser().thenReturn(parser_mock)
    when(parser_mock).add_argument(*args, **kwargs)
    _prepare_parser(defaults)
    default_values = [value for value in CLI_DEFAULTS.values() if not isinstance(value, dict)]
    for value in default_values:
        verify(parser_mock, atleast=1).add_argument(*args, default=value, **kwargs)
