# SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
import pytest
from mockito import when, args
from mockito.mocking import Mock


@pytest.fixture
def un_stub():
    """Clears any mocks once the test has finished"""
    from mockito import unstub

    yield
    unstub()


def add_context_manager(mock: Mock) -> Mock:
    """
    Add possibility to use a mock within a ContextManager, e.g. `with <mock> as m:`

    Args:
        mock: to be made fit for ContextManager

    Returns:
        enhanced mock
    """
    when(mock).__enter__().thenReturn(mock)
    when(mock).__exit__(*args).thenReturn()
    return mock


@pytest.fixture
def change_dir(request, monkeypatch):
    """Changes the path of this test to the directory of the test file"""
    monkeypatch.chdir(request.fspath.dirname)
