# SPDX-FileCopyrightText: 2023 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
from pathlib import Path

import pytest

from fameio.tools import ensure_is_list, keys_to_lower, clean_up_file_name, ensure_path_exists


@pytest.fixture
def mock_path(tmp_path):
    return Path(tmp_path, "file.yaml")


class Test:
    @pytest.mark.parametrize(
        "given, expected",
        [
            ({"all_lower": 42}, {"all_lower": 42}),
            ({"ALL_UPPER_DICT": 42}, {"all_upper_dict": 42}),
            ({"MiXedCaPS-LiSt": 42}, {"mixedcaps-list": 42}),
            ({}, {}),
        ],
    )
    def test__keys_to_lower__returns_keys_lower(self, given, expected):
        assert keys_to_lower(given) == expected

    @pytest.mark.parametrize(
        "given, expected",
        [
            (5, [5]),
            (9000.1, [9000.1]),
            ("List", ["List"]),
            ({"Answer": 42}, [{"Answer": 42}]),
            ({}, [{}]),
            (None, [None]),
        ],
    )
    def test__ensure_is_list__no_list_given__returns_list_with_given_value(self, given, expected):
        assert ensure_is_list(given) == expected

    @pytest.mark.parametrize(
        "given, expected",
        [
            ([5, 7], [5, 7]),
            ([9000.1], [9000.1]),
            (["List", "Items"], ["List", "Items"]),
            ([], []),
            ([{"Answer": 42}], [{"Answer": 42}]),
            ([None], [None]),
            ([None, 2, "a", 8.99, {}], [None, 2, "a", 8.99, {}]),
        ],
    )
    def test__ensure_is_list__list_given__returns_received_list(self, given, expected):
        assert ensure_is_list(given) == expected

    @pytest.mark.parametrize(
        "given, expected",
        [
            ("test string", "test_string"),
            ("te:st", "te_st"),
            ("te/st:str ing", "te-st_str_ing"),
            ("test_string", "test_string"),
            ("", ""),
            ("test", "test"),
        ],
    )
    def test_clean_up_file_name(self, given, expected):
        assert expected == clean_up_file_name(given)

    def test_ensure_path_exists(self, tmp_path):
        file_path = Path(tmp_path, "file.yaml")
        assert not file_path.exists()
        ensure_path_exists(file_path)
        assert file_path.exists()
