# SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0

import io
import logging as log
from typing import IO

import pytest
from fameprotobuf.data_storage_pb2 import DataStorage

from fameio.output.reader import Reader, ReaderV2
from tests.log_utils import fame_log
from tests.utils import assert_exception_contains

RANDOM_BINARY = b"pafdgoasdeASFLkmsefmgdso3"  # noqa
EMPTY_BINARY = b""
HEADER_V1 = b"famecoreprotobufstreamfilev001"  # noqa
HEADER_V2 = b"fameprotobufstreamfilev002    "  # noqa


def make_storage_string() -> bytes:
    """Returns serialised DataStorage"""
    data_storage = DataStorage()
    output = data_storage.output
    agent_type = output.agent_types.add()
    agent_type.class_name = "MyAgent"
    return data_storage.SerializeToString()


class DummyReader(Reader):
    """For testing of ABC methods"""

    def read(self):
        pass


class TestReader:
    @pytest.mark.parametrize("header", [b"notTheHeader", EMPTY_BINARY, RANDOM_BINARY])
    def test__get_reader__no_or_wrong_header_v0__raises(self, fame_log, header):
        file = io.BytesIO(header)
        with pytest.raises(Exception) as e_info:
            Reader.get_reader(file)
        fame_log.assert_logged_exception(Reader._ERR_DEPRECATED_V0, e_info)

    def test__get_reader__header_v1__raises(self, fame_log):
        file = io.BytesIO(HEADER_V1)
        with pytest.raises(Exception) as e_info:
            Reader.get_reader(file)
        fame_log.assert_logged_exception(Reader._ERR_DEPRECATED_V1, e_info)

    def test__get_reader__v2_single_mode__is_single_mode(self):
        file = io.BytesIO(HEADER_V2)
        reader = Reader.get_reader(file, read_single=True)
        assert reader._read_single

    def test_read_message_length_end_of_file(self, fame_log):
        fame_log.set_level(log.DEBUG)
        file = io.BytesIO(EMPTY_BINARY)
        message_length = DummyReader(file, False)._read_message_length()
        fame_log.assert_logged(Reader._DEBUG_FILE_END_REACHED)
        assert message_length == 0

    def test_read_message_length_positive(self):
        binary_string = (555).to_bytes(4, byteorder="big", signed=True)
        file = io.BytesIO(binary_string)
        message_length = DummyReader(file, False)._read_message_length()
        assert 555 == message_length

    def test_read_message_length_file_pointer_incremented(self):
        binary_string = (555).to_bytes(4, byteorder="big", signed=True)
        file = io.BytesIO(binary_string + b"some_random_stuff")
        DummyReader(file, False)._read_message_length()
        assert file.tell() == Reader.BYTES_DEFINING_MESSAGE_LENGTH

    def test_read_data_storage_message_length_negative_length(self):
        file = io.BytesIO(b"lelalu")  # noqa
        with pytest.raises(IOError) as e_info:
            DummyReader(file, False)._read_data_storage_message(-2)
        assert_exception_contains(Reader._ERR_FILE_CORRUPT_NEGATIVE_LENGTH, e_info)

    def test_read_data_storage_message_missing_data_log_error(self, fame_log):
        binary_string = make_storage_string()
        file = io.BytesIO(binary_string)
        message_length = len(binary_string) + 100
        DummyReader(file, False)._read_data_storage_message(message_length)
        fame_log.assert_logged(Reader._ERR_FILE_CORRUPT_MISSING_DATA)

    def test_read_data_storage_message_no_length_read_all(self):
        binary_string = make_storage_string()
        file = io.BytesIO(binary_string)
        DummyReader(file, False)._read_data_storage_message()
        assert len(binary_string) == file.tell()

    def test_read_data_storage_message_read_given_number_of_bytes(self):
        binary_string = make_storage_string()
        file = io.BytesIO(binary_string + b"otherStuffNotToBeRead")
        DummyReader(file, False)._read_data_storage_message(len(binary_string))
        assert len(binary_string) == file.tell()

    def test_read_data_storage_message_returns_none(self):
        file = io.BytesIO(EMPTY_BINARY)
        assert DummyReader(file, False).read() is None


class TestReaderV2:
    def test__read__broken_message__raises(self):
        lengths, file = self._build_data_storage_binary_stream(1, RANDOM_BINARY)
        with pytest.raises(IOError) as e_info:
            ReaderV2(file, False).read()
        assert_exception_contains(Reader._ERR_PARSING_FAILED, e_info)

    @staticmethod
    def _build_data_storage_binary_stream(number_of_storages: int, message: bytes) -> tuple[list[int], IO]:
        """Returns a binary stream of one or multiple data storages (prefixed by their byte length)"""
        stream = b""
        lengths = []
        for count in range(0, number_of_storages):
            stream += len(message).to_bytes(4, byteorder="big", signed=True)
            stream += message
            lengths.append(len(message))
        return lengths, io.BytesIO(stream)

    def test__read__one_message__returns_one_data_storage(self):
        lengths, file = self._build_data_storage_binary_stream(1, make_storage_string())
        result = ReaderV2(file, False).read()
        assert len(result) == 1
        assert file.tell() == lengths[0] + Reader.BYTES_DEFINING_MESSAGE_LENGTH
        assert isinstance(result[0], DataStorage)

    def test__read__multiple_messages__returns_multiple_data_storages(self):
        lengths, file = self._build_data_storage_binary_stream(3, make_storage_string())
        result = ReaderV2(file, False).read()
        assert len(result) == 3
        assert file.tell() == 3 * Reader.BYTES_DEFINING_MESSAGE_LENGTH + sum(lengths)
        assert all([isinstance(item, DataStorage) for item in result])

    def test__read__in_memory_saving_mode__returns_one_data_storage(self):
        lengths, file = self._build_data_storage_binary_stream(3, make_storage_string())
        result = ReaderV2(file, True).read()
        assert len(result) == 1
        assert file.tell() == Reader.BYTES_DEFINING_MESSAGE_LENGTH + lengths[0]

    def test__read__in_memory_saving_mode__returns_empty_list_at_end(self):
        _, file = self._build_data_storage_binary_stream(1, make_storage_string())
        reader = ReaderV2(file, True)
        reader.read()
        result = reader.read()
        assert len(result) == 0
