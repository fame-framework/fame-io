# SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0

import pytest
from fameprotobuf.contract_pb2 import ProtoContract
from fameprotobuf.input_file_pb2 import InputData
from mockito import mock, when, any, verify

from fameio.output.input_dao import InputDao, InputConversionError
from fameio.input.scenario import GeneralProperties, Contract, Agent, Attribute
from fameio.input.schema import AttributeType
from fameio.series import TimeSeriesManager
from tests.mock_utils import un_stub
from tests.output.input_mocks import mock_data_storage, mock_input_data, mock_series, mock_agent, mock_attribute
from tests.utils import assert_exception_contains

_SAMPLE_SCHEMA_STRING = repr(
    {
        "JavaPackages".lower(): {
            "Agents".lower(): ["MyAgentPackage"],
            "DataItems".lower(): ["MyDataItemPackage"],
            "Portables".lower(): ["MyPortable"],
        },
        "AgentTypes".lower(): {
            "MyAgent": {
                "Attributes": {
                    "EnumVal": {"AttributeType": "enum", "Mandatory": False, "List": False, "Values": ["A", "B", "C"]},
                    "TimeSeriesVal": {
                        "AttributeType": "time_series",
                        "Mandatory": False,
                        "List": False,
                    },
                    "DoubleVal": {
                        "AttributeType": "double",
                        "Mandatory": False,
                        "List": False,
                    },
                    "IntegerVal": {
                        "AttributeType": "integer",
                        "Mandatory": False,
                        "List": False,
                    },
                    "StringVal": {
                        "AttributeType": "string",
                        "Mandatory": False,
                        "List": False,
                    },
                    "LongVal": {
                        "AttributeType": "long",
                        "Mandatory": False,
                        "List": False,
                    },
                    "DoubleListVal": {
                        "AttributeType": "double",
                        "Mandatory": False,
                        "List": True,
                    },
                    "BlockVal": {
                        "AttributeType": "block",
                        "Mandatory": False,
                        "List": False,
                        "NestedAttributes": {
                            "IntegerVal": {
                                "AttributeType": "integer",
                                "Mandatory": False,
                                "List": False,
                            },
                        },
                    },
                    "BlockListVal": {
                        "AttributeType": "block",
                        "Mandatory": False,
                        "List": True,
                        "NestedAttributes": {
                            "IntegerVal": {
                                "AttributeType": "integer",
                                "Mandatory": False,
                                "List": False,
                            },
                        },
                    },
                }
            }
        },
    }
)


class TestInputDao:
    def test__store_inputs__list_given__those_with_input_saved(self, un_stub):
        data_storages = [mock_data_storage(True), mock_data_storage(False), mock_data_storage(True)]
        input_dao = InputDao()
        input_dao.store_inputs(data_storages)
        assert len(input_dao._inputs) == 2

    def test__store_inputs__multiple_times__those_with_input_saved(self, un_stub):
        input_dao = InputDao()
        input_dao.store_inputs([mock_data_storage(True)])
        input_dao.store_inputs([mock_data_storage(False), mock_data_storage(True)])
        input_dao.store_inputs([mock_data_storage(False)])
        assert len(input_dao._inputs) == 2

    def test__get_input_data__empty__raises(self):
        input_dao = InputDao()
        with pytest.raises(InputConversionError) as e_info:
            input_dao._get_input_data()
        assert_exception_contains(InputDao._ERR_NO_INPUTS, e_info)

    def test__get_input_data__more_than_one__raises(self, un_stub):
        input_dao = InputDao()
        input_dao._inputs = [mock(InputData), mock(InputData)]
        with pytest.raises(InputConversionError) as e_info:
            input_dao._get_input_data()
        assert_exception_contains(InputDao._ERR_MULTIPLE_INPUTS, e_info)

    def test__get_input_data__exactly_one__returns(self, un_stub):
        input_dao = InputDao()
        input_data: InputData = mock(InputData)
        input_dao._inputs = [input_data]
        assert input_dao._get_input_data() == input_data

    def test__get_schema__created_from_string(self, un_stub):
        input_dao = InputDao()
        schema = input_dao._get_schema(mock_input_data(schema=_SAMPLE_SCHEMA_STRING))
        assert schema.to_string() == _SAMPLE_SCHEMA_STRING

    def test__get_general_properties__restores_general_properties(self, un_stub):
        input_dao = InputDao()
        original = GeneralProperties(
            run_id=1, simulation_start_time=0, simulation_stop_time=1000, simulation_random_seed=1
        )
        general_properties = input_dao._get_general_properties(mock_input_data(general_properties=original))
        assert general_properties.to_dict() == original.to_dict()

    def test__get_contracts__restores_contracts(self, un_stub):
        input_dao = InputDao()
        contract1 = Contract(1, 2, "ProductA", 3600, 0, 1000, {"Meta": "A"})
        contract2 = Contract(2, 1, "ProductB", 3600, 1, 1000, {"Meta": "B"})
        contract_mocks = [self.mock_contract(contract1), self.mock_contract(contract2)]

        contracts = input_dao._get_contracts(mock_input_data(contracts=contract_mocks))
        assert contracts[0].to_dict() == contract1.to_dict()
        assert contracts[1].to_dict() == contract2.to_dict()

    @staticmethod
    def mock_contract(contract: Contract) -> ProtoContract:
        """Returns a mocked ProtoContract created from an original one"""
        proto: ProtoContract = mock(ProtoContract)
        proto.sender_id = contract.sender_id
        proto.receiver_id = contract.receiver_id
        proto.product_name = contract.product_name
        proto.first_delivery_time = contract.first_delivery_time
        proto.delivery_interval_in_steps = contract.delivery_interval
        proto.expiration_time = contract.expiration_time
        proto.metadata = contract.get_metadata_string()
        return proto

    def test__init_timeseries__timeseries_manager_is_called(self, un_stub):
        input_dao = InputDao()
        timeseries_manager_mock: TimeSeriesManager = mock(TimeSeriesManager)
        when(timeseries_manager_mock).reconstruct_time_series(any)
        input_dao._timeseries_manager = timeseries_manager_mock
        series1 = mock_series(series_id=0, series_name="SeriesOne", data=[(0, 1)])
        series2 = mock_series(series_id=1, series_name="Load", data=[(0, 55.2), (1, 54.3), (3, 99.0)])
        input_dao._init_timeseries(mock_input_data(series=[series1, series2]))
        verify(timeseries_manager_mock, times=1).reconstruct_time_series(...)

    def test__get_agents__single_with_timeseries__returns_agent(self, un_stub):
        input_dao = InputDao()
        timeseries_manager_mock: TimeSeriesManager = mock(TimeSeriesManager)
        when(timeseries_manager_mock).get_reconstructed_series_by_id(0).thenReturn("MySeries.csv")
        input_dao._timeseries_manager = timeseries_manager_mock
        original = Agent(agent_id=1, type_name="MyAgent", metadata={"MyMeta": "some text"})
        original.add_attribute("TimeSeriesVal", Attribute("TimeSeriesVal", "MySeries.csv"))
        agent = mock_agent(
            agent=original,
            attributes=[mock_attribute(name="TimeSeriesVal", attribute_type=AttributeType.TIME_SERIES, value=0)],
        )

        input_data = mock_input_data(schema=_SAMPLE_SCHEMA_STRING, agents=[agent])
        input_dao._schema = input_dao._get_schema(input_data)
        result = input_dao._get_agents(input_data)[0]
        assert result.to_dict() == original.to_dict()

    @pytest.mark.parametrize(
        "params",
        [
            {"name": "StringVal", "type": AttributeType.STRING, "value": "test"},
            {"name": "EnumVal", "type": AttributeType.ENUM, "value": "A"},
            {"name": "DoubleVal", "type": AttributeType.DOUBLE, "value": 42.0},
            {"name": "IntegerVal", "type": AttributeType.INTEGER, "value": 2},
            {"name": "LongVal", "type": AttributeType.LONG, "value": 500},
            {"name": "DoubleListVal", "type": AttributeType.DOUBLE, "value": [0.1, 0.2]},
        ],
    )
    def test__get_agents__single_with_attribute__returns_agent(self, params, un_stub):
        input_dao = InputDao()
        original_agent = Agent(agent_id=1, type_name="MyAgent", metadata={"MyMeta": "some text"})
        original_agent.add_attribute(
            name=params["name"], value=Attribute(name=params["name"], definitions=params["value"])
        )
        agent = mock_agent(
            agent=original_agent, attributes=[mock_attribute(params["name"], params["type"], params["value"])]
        )
        input_data = mock_input_data(schema=_SAMPLE_SCHEMA_STRING, agents=[agent])
        input_dao._schema = input_dao._get_schema(input_data)

        restored_agent = input_dao._get_agents(input_data)[0]
        assert restored_agent.to_dict() == original_agent.to_dict()

    def test__get_agents__single_with_block__returns_agent(self, un_stub):
        input_dao = InputDao()
        original_agent = Agent(agent_id=1, type_name="MyAgent", metadata={"MyMeta": "some text"})
        original_agent.add_attribute(name="BlockVal", value=Attribute(name="BlockVal", definitions={"IntegerVal": 5}))
        agent = mock_agent(
            agent=original_agent,
            attributes=[
                mock_attribute(
                    name="BlockVal",
                    attribute_type=AttributeType.BLOCK,
                    value=mock_attribute(name="IntegerVal", attribute_type=AttributeType.INTEGER, value=5),
                )
            ],
        )
        input_data = mock_input_data(schema=_SAMPLE_SCHEMA_STRING, agents=[agent])
        input_dao._schema = input_dao._get_schema(input_data)

        restored_agent = input_dao._get_agents(input_data)[0]
        assert restored_agent.to_dict() == original_agent.to_dict()

    def test__get_agents__single_with_block_list__returns_agent(self, un_stub):
        input_dao = InputDao()
        original = Agent(agent_id=1, type_name="MyAgent", metadata={"MyMeta": "some text"})
        original.add_attribute(
            name="BlockListVal",
            value=Attribute(name="BlockListVal", definitions=[{"IntegerVal": 5}, {"IntegerVal": 17}]),
        )
        agent = mock_agent(
            agent=original,
            attributes=[
                mock_attribute(
                    name="BlockListVal",
                    attribute_type=AttributeType.BLOCK,
                    value=[
                        mock_attribute(name="IntegerVal", attribute_type=AttributeType.INTEGER, value=5),
                        mock_attribute(name="IntegerVal", attribute_type=AttributeType.INTEGER, value=17),
                    ],
                )
            ],
        )
        input_data = mock_input_data(schema=_SAMPLE_SCHEMA_STRING, agents=[agent])
        input_dao._schema = input_dao._get_schema(input_data)
        result = input_dao._get_agents(input_data)[0]
        assert result.to_dict() == original.to_dict()

    def test__recover_inputs__returns_timeseries_manager_and_scenario(self, un_stub):
        input_dao = InputDao()
        timeseries_manager_mock: TimeSeriesManager = mock(TimeSeriesManager)
        input_dao._timeseries_manager = timeseries_manager_mock
        when(timeseries_manager_mock).reconstruct_time_series(any)
        general_properties = GeneralProperties(
            run_id=1,
            simulation_start_time=0,
            simulation_stop_time=1000,
            simulation_random_seed=1,
        )

        original_agent = Agent(agent_id=1, type_name="MyAgent", metadata={"MyMeta": "some text"})
        original_agent.add_attribute(name="IntegerVal", value=Attribute(name="IntegerVal", definitions=2))
        agent_mock = mock_agent(
            agent=original_agent,
            attributes=[mock_attribute(name="IntegerVal", attribute_type=AttributeType.INTEGER, value=2)],
        )

        original_contract = Contract(1, 1, "ProductA", 3600, 0, 1000, {"Meta": "A"})
        contract_mock = self.mock_contract(original_contract)
        input_dao._inputs = [
            mock_input_data(
                general_properties=general_properties,
                schema=_SAMPLE_SCHEMA_STRING,
                agents=[agent_mock],
                contracts=[contract_mock],
            )
        ]
        manager, scenario = input_dao.recover_inputs()
        assert manager == timeseries_manager_mock
        assert scenario.agents[0].to_dict() == original_agent.to_dict()
        assert scenario.contracts[0].to_dict() == original_contract.to_dict()
