# SPDX-FileCopyrightText: 2023 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
import pandas as pd
import pytest
from mockito import mock, verify, times
from pandas.testing import assert_frame_equal

import fameio
from fameio.cli.options import TimeOptions

# noinspection PyProtectedMember
from fameio.output.conversion import (
    apply_time_option,
    merge_time,
    apply_time_merging,
    _ERR_NEGATIVE,
)
from fameio.output.data_transformer import INDEX
from fameio.time import ConversionError
from tests.mock_utils import un_stub
from tests.output.test_csv_writer import create_data_frame, create_data_frame_with_additional_index_column
from tests.utils import assert_exception_contains


class TestTimeConversion:
    def test__apply_time_option__int__keeps_integer_time(self):
        data = {None: create_data_frame(agents=1, times=3, columns=["A", "B", "C"])}
        apply_time_option(data, mode=TimeOptions.INT)
        for time in data[None].reset_index()["TimeStep"]:
            assert isinstance(time, int)

    def test__apply_time_option__utc__converted(self):
        data = {None: create_data_frame(agents=1, times=3, columns=["A", "B", "C"])}
        apply_time_option(data, mode=TimeOptions.UTC)
        for time in data[None].reset_index()["TimeStep"]:
            assert " " in time

    def test__apply_time_option__fame__converted(self):
        data = {None: create_data_frame(agents=1, times=3, columns=["A", "B", "C"])}
        apply_time_option(data, mode=TimeOptions.FAME)
        for time in data[None].reset_index()["TimeStep"]:
            assert "_" in time

    @pytest.mark.parametrize("mode_name", ["invalid_mode", "lalelu", "UTCA", "FAMES", "uint"])
    def test__apply_time_option__invalid_mode__raises(self, mode_name):
        data = {None: create_data_frame(agents=1, times=3, columns=["A", "B", "C"])}
        with pytest.raises(ConversionError) as e_info:
            # noinspection PyTypeChecker
            apply_time_option(data, mode=mode_name)
        assert_exception_contains(fameio.output.conversion._ERR_UNIMPLEMENTED, e_info)

    def test__convert_time_index__utc__yields_utc_format(self):
        data = {None: create_data_frame(agents=1, times=3, columns=["A"])}
        expected = pd.DataFrame.from_dict(
            {
                (0, "2000-01-01 00:00:00"): {"A": 0},
                (0, "2000-01-01 00:00:01"): {"A": 0},
                (0, "2000-01-01 00:00:02"): {"A": 0},
            },
            orient="index",
        ).rename_axis(INDEX)
        fameio.output.conversion._convert_time_index(data, datetime_format="%Y-%m-%d %H:%M:%S")
        for _, df1 in data.items():
            assert_frame_equal(df1, expected)

    def test__convert_time_index__fame__yields_fame_format(self):
        data = {None: create_data_frame(agents=1, times=3, columns=["A"])}
        expected = pd.DataFrame.from_dict(
            {
                (0, "2000-01-01_00:00:00"): {"A": 0},
                (0, "2000-01-01_00:00:01"): {"A": 0},
                (0, "2000-01-01_00:00:02"): {"A": 0},
            },
            orient="index",
        ).rename_axis(INDEX)
        fameio.output.conversion._convert_time_index(data, datetime_format="%Y-%m-%d_%H:%M:%S")
        for _, df1 in data.items():
            assert_frame_equal(df1, expected)

    def test__convert_time_index__multiindex_fame__yields_fame_format(self):
        data = {None: create_data_frame_with_additional_index_column(1, 3, ["A"], "Plant")}
        expected = pd.DataFrame.from_dict(
            {
                (0, "2000-01-01_00:00:00", "Plant"): {"A": 0},
                (0, "2000-01-01_00:00:01", "Plant"): {"A": 0},
                (0, "2000-01-01_00:00:02", "Plant"): {"A": 0},
            },
            orient="index",
        ).rename_axis((["AgentId", "TimeStep", "Plant"]))
        fameio.output.conversion._convert_time_index(data, datetime_format="%Y-%m-%d_%H:%M:%S")
        for _, df1 in data.items():
            assert_frame_equal(df1, expected)

    @pytest.mark.parametrize(
        "t, focal_point, offset, interval, expected",
        [
            (0, 0, 0, 1, 0),
            (17, 0, 0, 1, 17),
            (-17, 0, 0, 1, -17),
            (8, 0, 5, 10, 10),
            (5, 0, 5, 10, 10),
            (14, 0, 5, 10, 10),
            (24, 0, 5, 10, 20),
            (26, 0, 5, 10, 30),
            (-7, 2, 0, 3, -7),
            (-6, 2, 0, 3, -7),
            (-8, 2, 0, 3, -10),
            (-1, 2, 0, 3, -1),
            (-2, 2, 0, 3, -4),
            (0, 2, 0, 3, -1),
            (1, 2, 0, 3, -1),
            (2, 2, 0, 3, 2),
            (3, 2, 0, 3, 2),
            (4, 2, 0, 3, 2),
            (5, 2, 0, 3, 5),
            (4, 2, 1, 3, 5),
            (5, 2, 1, 3, 5),
            (6, 2, 1, 3, 5),
            (-6, 2, 2, 3, -4),
            (-7, 2, 2, 3, -7),
            (-8, 2, 2, 3, -7),
            (-6, 2, 1, 3, -7),
            (-7, 2, 1, 3, -7),
            (-8, 2, 1, 3, -7),
        ],
    )
    def test__merge_time__yields_correct_result(self, t, focal_point, offset, interval, expected):
        assert merge_time(t, focal_point, offset, interval) == expected

    def test__apply_time_merging__no_config__no_action(self, un_stub):
        data = mock(dict)
        apply_time_merging(data, None)
        verify(data, times(0)).keys()

    def test__apply_time_merging__meaningless_config__no_action(self, un_stub):
        data = mock(dict)
        apply_time_merging(data, [0, 0, 0])
        verify(data, times(0)).keys()

    @pytest.mark.parametrize("config", [[0, -1, 1], [0, 1, -1], [0, -5, -2]])
    def test__apply_time_merging__negative_steps__raises(self, config):
        with pytest.raises(ValueError) as e_info:
            apply_time_merging({}, config)
        assert_exception_contains(_ERR_NEGATIVE, e_info)

    def test__apply_time_merging__one_dataframe__adapts_data(self):
        data = {None: create_data_frame(agents=2, times=3, columns=["A", "B"])}
        config = [2, 1, 0]
        apply_time_merging(data, config)
        expected = pd.DataFrame.from_dict(
            {
                (0, 0): {"A": 0, "B": 0},
                (0, 2): {"A": 0, "B": 0},
                (1, 0): {"A": 0, "B": 0},
                (1, 2): {"A": 0, "B": 2 + 1},
            },
            orient="index",
        ).rename_axis(INDEX)
        for _, df1 in data.items():
            assert_frame_equal(df1, expected)

    def test__apply_time_merging__empty_dataframe__returns_empty(self):
        index = pd.MultiIndex.from_arrays([[], []], names=INDEX)
        data = {None: pd.DataFrame(index=index)}
        config = [2, 1, 0]
        apply_time_merging(data, config)
        expected = pd.DataFrame(index=index)
        for _, df1 in data.items():
            df1.equals(expected)

    def test__apply_time_merging__multiple_dataframes__adapts_all(self):
        data = {
            "First": create_data_frame(agents=2, times=3, columns=["A", "B"]),
            "Second": create_data_frame_with_additional_index_column(2, 4, ["A", "B"], "Plant"),
        }
        config = [2, 1, 0]
        apply_time_merging(data, config)
        expected = {
            "First": pd.DataFrame.from_dict(
                {
                    (0, 0): {"A": 0, "B": 0},
                    (0, 2): {"A": 0, "B": 0},
                    (1, 0): {"A": 0, "B": 0},
                    (1, 2): {"A": 0, "B": 2 + 1},
                },
                orient="index",
            ).rename_axis(INDEX),
            "Second": pd.DataFrame.from_dict(
                {
                    (0, 0, "Plant"): {"A": 0, "B": 0},
                    (0, 2, "Plant"): {"A": 0, "B": 0},
                    (0, 4, "Plant"): {"A": 0, "B": 0},
                    (1, 0, "Plant"): {"A": 0, "B": 0},
                    (1, 2, "Plant"): {"A": 0, "B": 2 + 1},
                    (1, 4, "Plant"): {"A": 0, "B": 3},
                },
                orient="index",
            ).rename_axis(INDEX + ("Plant",)),
        }
        for (_, d), (_, e) in zip(data.items(), expected.items()):
            assert_frame_equal(d, e)
