# SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
from typing import Optional, Any

from fameprotobuf.contract_pb2 import ProtoContract
from fameprotobuf.data_storage_pb2 import DataStorage
from fameprotobuf.field_pb2 import NestedField
from fameprotobuf.input_file_pb2 import InputData
from mockito import mock, when

from fameio.output.input_dao import InputDao
from fameio.input.scenario import Agent, GeneralProperties
from fameio.input.schema import AttributeType


def mock_data_storage(has_input: bool) -> DataStorage:
    """Returns a mocked DataStorage with an empty mocked InputData (if `has_input`) is True"""
    data_storage: DataStorage = mock(DataStorage)
    when(data_storage).HasField("input").thenReturn(has_input)
    if has_input:
        data_storage.input = mock(InputData)
    return data_storage


def mock_input_data(
    general_properties: Optional[GeneralProperties] = None,
    schema: Optional[str] = None,
    agents: Optional[list[InputData.AgentDao]] = None,
    contracts: Optional[list[ProtoContract]] = None,
    series: Optional[list[InputData.TimeSeriesDao]] = None,
) -> InputData:
    """Mocks an InputData object with provided content"""
    input_data: InputData = mock(InputData)
    if general_properties:
        input_data.run_id = general_properties.run_id
        input_data.simulation = mock(InputData.SimulationParam)
        input_data.simulation.start_time = general_properties.simulation_start_time
        input_data.simulation.stop_time = general_properties.simulation_stop_time
        input_data.simulation.random_seed = general_properties.simulation_random_seed
    if schema:
        input_data.schema = schema
    input_data.agents = agents if agents else []
    input_data.contracts = contracts if contracts else []
    input_data.time_series = series if series else []
    return input_data


def mock_series(series_id: int, series_name: str, data: list[tuple[int, float]]) -> InputData.TimeSeriesDao:
    """Returns a mocked TimeSeriesDao created from given parameters"""
    proto: InputData.TimeSeriesDao = mock(InputData.TimeSeriesDao)
    proto.series_id = series_id
    proto.series_name = series_name
    proto.time_steps = [entry[0] for entry in data]
    proto.values = [entry[1] for entry in data]
    return proto


def mock_agent(agent: Agent, attributes: Optional[list[NestedField]]) -> InputData.AgentDao:
    """Returns a mocked AgentDao created from given Agent plus given attributes"""
    proto: InputData.AgentDao = mock(InputData.AgentDao)
    proto.id = agent.id
    proto.class_name = agent.type_name
    proto.metadata = agent.get_metadata_string()
    proto.fields = attributes
    return proto


def mock_attribute(name: str, attribute_type: AttributeType, value: Any) -> NestedField:
    """Returns mocked NestedField from given parameters"""
    proto: NestedField = mock(NestedField)
    proto.field_name = name
    if (attribute_type is not AttributeType.TIME_SERIES) and (not isinstance(value, list)):
        value = [value]
    elif (attribute_type is AttributeType.BLOCK) and (isinstance(value, list)):
        value = [mock_attribute(str(index), AttributeType.BLOCK, value=entry) for index, entry in enumerate(value)]
    # noinspection PyProtectedMember
    proto.__setattr__(InputDao._FIELD_NAME_MAP[attribute_type], value)
    return proto
