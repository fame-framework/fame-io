# SPDX-FileCopyrightText: 2025 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
import builtins
import logging
from io import TextIOWrapper
from pathlib import Path

import pytest
import yaml
from mockito import any, when, mock, verify

from fameio.output.yaml_writer import data_to_yaml_file, ERR_WRITE_EXCEPTION, INFO_DESTINATION
from tests.log_utils import fame_log
from tests.mock_utils import un_stub, add_context_manager
from tests.utils import assert_exception_contains


@pytest.fixture
def stub_writing():
    io_mock = add_context_manager(mock(TextIOWrapper))
    when(builtins).open(Path(), "w", encoding="utf-8").thenReturn(io_mock)
    when(yaml).dump(any, io_mock, sort_keys=False, encoding="utf-8")


class TestYamlWriter:
    def test__data_to_yaml_file__info_logged(self, fame_log, stub_writing, un_stub):
        fame_log.set_level(logging.INFO)
        dummy_data = {"A": 42}
        data_to_yaml_file(dummy_data, Path())
        fame_log.assert_logged(INFO_DESTINATION)

    def test__data_to_yaml_file__on_error__raise(self):
        dummy_data = {"A": 44}
        with pytest.raises(RuntimeError) as e_info:
            data_to_yaml_file(dummy_data, Path("invalid/path"))
            assert_exception_contains(ERR_WRITE_EXCEPTION, e_info)

    def test__data_to_yaml_file__calls_dump_with_data(self, stub_writing, un_stub):
        dummy_data = {"A": 42}
        data_to_yaml_file(dummy_data, Path())
        verify(yaml, times=1).dump(...)
