# SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
import os
from pathlib import Path

import pandas as pd
import pytest
from mockito import mock, when, verify, any

from fameio.output.csv_writer import CsvWriter
from fameio.output.data_transformer import INDEX
from fameio.series import TimeSeriesManager
from tests.mock_utils import un_stub


@pytest.fixture
def timeseries_manager():
    return mock(TimeSeriesManager)


def create_data_frame(agents: int, times: int, columns: list[str]) -> pd.DataFrame:
    """
    Creates a dataframe with given count of `agents`, each with `times` rows and given `columns`
    Each value is the product of agent_id, time and column index
    """
    output = {
        (agent_id, time): {column_name: agent_id * time * i for i, column_name in enumerate(columns)}
        for agent_id in range(agents)
        for time in range(times)
    }
    data_frame = pd.DataFrame.from_dict(output, orient="index")
    data_frame.rename_axis(INDEX, inplace=True)
    return data_frame


def create_data_frame_with_additional_index_column(
    agents: int, times: int, columns: list[str], idx_col: str
) -> pd.DataFrame:
    """
    Creates a dataframe with MultiIndex ("AgentId", "TimeStep", `idx_col`) with given count of `agents`,
    each with `times` rows and given `columns`. Each value is the product of agent_id, time and column index.
    """
    output = {
        (agent_id, time, idx_col): {column_name: agent_id * time * i for i, column_name in enumerate(columns)}
        for agent_id in range(agents)
        for time in range(times)
    }
    data_frame = pd.DataFrame.from_dict(output, orient="index")
    data_frame.rename_axis(INDEX + (idx_col,), inplace=True)
    return data_frame


def assert_exists_and_matching(expected_file: Path, expected_data: pd.DataFrame) -> None:
    """Asserts that given `expected_file` exists and contains identical data to given dataframe `compare_to`"""
    assert expected_file.exists()
    parsed_df = pd.read_csv(expected_file, sep=";", index_col=INDEX)
    pd.testing.assert_frame_equal(expected_data, parsed_df)


class TestCsvWriter:
    def test_init_creates_specified_folder(self, tmp_path):
        directory = Path(tmp_path, "myFolder")
        file = Path(tmp_path, "myResult.pb")
        CsvWriter(directory, file, False)
        assert directory.is_dir()

    def test_init_no_path_creates_folder_in_working_dir_from_filename(self, tmp_path):
        file = Path("/this/path/is/not/used/myResult.pb")
        os.chdir(tmp_path)
        # noinspection PyTypeChecker
        CsvWriter(None, file, False)
        assert (Path(tmp_path, "myResult")).is_dir()

    def test_init_keeps_existing_output_folder(self, tmp_path):
        file = Path(tmp_path, "myResult.pb")
        directory = Path(tmp_path, "myFolder")
        directory.mkdir()
        file_to_keep = Path(directory, "keep_this.txt")
        file_to_keep.write_text("Hello Mama")
        CsvWriter(directory, file, False)
        assert file_to_keep.exists()

    def test_write_to_files_multi_export_simple_columns(self, tmp_path):
        directory = Path(tmp_path, "myFolder")
        csv_writer = CsvWriter(directory, Path(tmp_path, "myResult.pb"), single_export=False)
        df = create_data_frame(2, 3, ["A", "B", "C"])
        csv_writer.write_to_files("myAgent", {None: df})
        assert_exists_and_matching(Path(directory, "myAgent.csv"), df)

    def test_write_to_files_multi_export_complex_columns(self, tmp_path):
        directory = Path(tmp_path, "myFolder")
        csv_writer = CsvWriter(directory, Path(tmp_path, "myResult.pb"), single_export=False)
        df = create_data_frame(2, 3, ["A", "B", "C"])
        df2 = create_data_frame(2, 3, ["A2", "B2", "C2"])
        csv_writer.write_to_files("myAgent", {None: df, "Complex": df2})
        assert_exists_and_matching(Path(directory, "myAgent.csv"), df)
        assert_exists_and_matching(Path(directory, "myAgent_Complex.csv"), df2)

    def test_write_to_files_single_export_simple_columns(self, tmp_path):
        directory = Path(tmp_path, "myFolder")
        csv_writer = CsvWriter(directory, Path(tmp_path, "myResult.pb"), single_export=True)
        df = create_data_frame(2, 3, ["A", "B", "C"])
        csv_writer.write_to_files("myAgent", {None: df})
        for agent_id, agent_data in df.groupby(INDEX[0]):
            assert_exists_and_matching(Path(directory, f"myAgent_{agent_id}.csv"), agent_data)

    def test_write_to_files_single_export_complex_columns(self, tmp_path):
        directory = Path(tmp_path, "myFolder")
        csv_writer = CsvWriter(directory, Path(tmp_path, "myResult.pb"), single_export=True)
        df = create_data_frame(2, 3, ["A", "B", "C"])
        df2 = create_data_frame(3, 5, ["A2", "B2", "C2"])
        csv_writer.write_to_files("myAgent", {None: df, "Complex": df2})
        for agent_id, agent_data in df.groupby(INDEX[0]):
            assert_exists_and_matching(Path(directory, f"myAgent_{agent_id}.csv"), agent_data)
        for agent_id, agent_data in df2.groupby(INDEX[0]):
            assert_exists_and_matching(Path(directory, f"myAgent_Complex_{agent_id}.csv"), agent_data)

    def test_write_to_files_multi_export_append(self, tmp_path):
        directory = Path(tmp_path, "myFolder")
        csv_writer = CsvWriter(directory, Path(tmp_path, "myResult.pb"), single_export=False)
        df = create_data_frame(2, 3, ["A", "B", "C"])
        csv_writer.write_to_files("myAgent", {None: df})
        df2 = create_data_frame(2, 1, ["A", "B", "C"])
        csv_writer.write_to_files("myAgent", {None: df2})
        expected_result = pd.concat([df, df2])
        assert_exists_and_matching(Path(directory, "myAgent.csv"), expected_result)

    def test_write_to_files_single_export_append(self, tmp_path):
        directory = Path(tmp_path, "myFolder")
        csv_writer = CsvWriter(directory, Path(tmp_path, "myResult.pb"), single_export=True)
        df = create_data_frame(2, 3, ["A", "B", "C"])
        csv_writer.write_to_files("myAgent", {None: df})
        df2 = create_data_frame(2, 1, ["A", "B", "C"])
        csv_writer.write_to_files("myAgent", {None: df2})
        expected_result = pd.concat([df, df2])
        for agent_id, agent_data in expected_result.groupby(INDEX[0]):
            assert_exists_and_matching(Path(directory, f"myAgent_{agent_id}.csv"), agent_data)

    def test_write_to_files_overwrite_existing_multi_export(self, tmp_path):
        directory = Path(tmp_path, "myFolder")
        csv_writer = CsvWriter(directory, Path(tmp_path, "myResult.pb"), single_export=False)
        df = create_data_frame(2, 3, ["A", "B", "C"])
        csv_writer.write_to_files("myAgent", {None: df})

        csv_writer = CsvWriter(directory, Path(tmp_path, "myResult.pb"), single_export=False)
        df2 = create_data_frame(2, 1, ["A", "B", "C"])
        csv_writer.write_to_files("myAgent", {None: df2})
        assert_exists_and_matching(directory / "myAgent.csv", df2)

    def test_write_to_files_overwrite_existing_single_export(self, tmp_path):
        directory = Path(tmp_path, "myFolder")
        csv_writer = CsvWriter(directory, Path(tmp_path, "myResult.pb"), single_export=True)
        df = create_data_frame(2, 3, ["A", "B", "C"])
        csv_writer.write_to_files("myAgent", {None: df})

        csv_writer = CsvWriter(directory, Path(tmp_path, "myResult.pb"), single_export=True)
        df2 = create_data_frame(2, 1, ["A", "B", "C"])
        csv_writer.write_to_files("myAgent", {None: df2})
        for agent_id, agent_data in df2.groupby(INDEX[0]):
            assert_exists_and_matching(directory / f"myAgent_{agent_id}.csv", agent_data)

    def test__write_time_series_to_disk__no_series__ignored(self, timeseries_manager, un_stub):
        when(timeseries_manager).get_all_series().thenReturn([])
        csv_writer = CsvWriter(Path(), Path(), False)
        csv_writer.write_time_series_to_disk(timeseries_manager)

    def test__write_time_series_to_disk__no_data__ignored(self, timeseries_manager, un_stub):
        when(timeseries_manager).get_all_series().thenReturn([(0, "no_data", None)])
        csv_writer = CsvWriter(Path(), Path(), False)
        csv_writer.write_time_series_to_disk(timeseries_manager)

    def test__write_time_series_to_disk__output_path__considered(self, timeseries_manager, un_stub):
        file = "mySerie.csv"
        output_path = "./some/path"
        series_id, series_name, mock_df = self.mock_series(series_id=0, name=file)
        when(timeseries_manager).get_all_series().thenReturn([(series_id, series_name, mock_df)])
        csv_writer = CsvWriter(Path(output_path), Path(), False)
        csv_writer.write_time_series_to_disk(timeseries_manager)
        verify(mock_df, times=1).to_csv(path_or_buf=Path(output_path, series_name), sep=";", header=None, index=None)

    @staticmethod
    def mock_series(series_id: int, name: str) -> tuple[int, str, pd.DataFrame]:
        df = mock(pd.DataFrame)
        when(df).to_csv(path_or_buf=any(Path), sep=";", header=None, index=None)
        return series_id, name, df

    @pytest.mark.parametrize(
        ("files_to_append", "expected"),
        [
            (None, {}),
            ((Path("dummy_path"), "my_identifier"), {"my_identifier": Path("dummy_path")}),
        ],
    )
    def test__pop_all_file_paths__returns_existing_files(self, files_to_append, expected):
        csv_writer = CsvWriter(Path("./some/path"), Path(), False)
        if files_to_append:
            csv_writer._save_outfile_name(*files_to_append)
        assert csv_writer.pop_all_file_paths() == expected

    def test__pop_all_file_paths__empties_file_dict(self):
        csv_writer = CsvWriter(Path("./some/path"), Path(), False)
        csv_writer._save_outfile_name(Path("dummy_path"), "my_identifier")
        csv_writer.pop_all_file_paths()
        assert csv_writer._files == {}
