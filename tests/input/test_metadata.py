# SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
import ast
import copy

import pytest

from fameio.input import InputError
from fameio.input.metadata import Metadata, MetadataComponent, ValueContainer
from tests.utils import assert_exception_contains


class Dummy(Metadata):
    def __init__(self, definitions=None, my_data=None) -> None:
        super().__init__(definitions)
        self._my_data: dict = my_data

    def _to_dict(self) -> dict:
        return self._my_data


class TestMetadata:
    metadata_test_variations = [
        {
            "Origin": "Paper",
            "Time": "Yesterday",
        },
        {
            "A": {"C": 4, "MetaData": "This is C"},
            "B": "Yesterday",
        },
    ]

    def test__init__none__has_no_metadata(self):
        dummy = Dummy()
        assert dummy.metadata == {}

    def test__init__empty__has_no_metadata(self):
        dummy = Dummy({})
        assert dummy.metadata == {}

    def test__init__dict_wrong_keys__has_no_metadata(self):
        dummy = Dummy({"Not": "a", "key": "that", "is": "correct"})
        assert dummy.metadata == {}

    @pytest.mark.parametrize("metadata", metadata_test_variations)
    def test__init__with_metadata__sets_metadata(self, metadata):
        data = {
            "Anything": {},
            Metadata.KEY_METADATA: metadata,
        }
        dummy = Dummy(data)
        assert dummy.metadata == metadata

    @pytest.mark.parametrize("metadata", metadata_test_variations)
    def test__init__with_metadata__removes_metadata_from_input(self, metadata):
        to_keep = {"Anything": {}}
        data = {Metadata.KEY_METADATA: metadata, **to_keep}
        Dummy(data)
        assert data == to_keep

    @pytest.mark.parametrize("metadata", metadata_test_variations)
    def test__extract_metadata__sets_metadata(self, metadata):
        data = {
            "Anything": {},
            Metadata.KEY_METADATA: metadata,
        }
        dummy = Dummy()
        dummy._extract_metadata(data)
        assert dummy.metadata == metadata

    @pytest.mark.parametrize("metadata", metadata_test_variations)
    def test__extract_metadata__removes_metadata_from_input(self, metadata):
        to_keep = {"Anything": {}}
        data = {Metadata.KEY_METADATA: metadata, **to_keep}
        dummy = Dummy()
        dummy._extract_metadata(data)
        assert data == to_keep

    def test__extrac_metadata__none__passes(self):
        dummy = Dummy()
        dummy._extract_metadata(None)
        assert dummy.metadata == {}

    def test__extrac_metadata__empty__passes(self):
        dummy = Dummy()
        dummy._extract_metadata({})
        assert dummy.metadata == {}

    @pytest.mark.parametrize("key", ["MetaData", "metaData", "metadata", "mEtAdAtA"])
    def test__extract_metadata__allowed_keys__sets_metadata(self, key):
        metadata = {
            "Origin": "Paper",
            "Time": "Yesterday",
        }
        data = {
            "Anything": {},
            key: metadata,
        }
        dummy = Dummy()
        dummy._extract_metadata(data)
        assert dummy.metadata == metadata

    def test__extract_metadata__wrong_key__no_metadata_set(self):
        metadata = {
            "Origin": "Paper",
            "Time": "Yesterday",
        }
        data = {
            "Anything": {},
            "WrongKey": metadata,
        }
        dummy = Dummy()
        dummy._extract_metadata(data)
        assert dummy.metadata == {}

    @pytest.mark.parametrize("assigned_value", [1, "Some String", 4.2, [1, 2, 3], ("Some", "Tuple")])
    def test__extract_metadata__not_dict__returns_empty_dict(self, assigned_value):
        dummy = Dummy()
        # noinspection PyTypeChecker
        dummy._extract_metadata(assigned_value)
        assert dummy.metadata == {}

    @pytest.mark.parametrize("metadata", metadata_test_variations)
    def test__get_metadata_string__metadata_present__returns_string_representation(self, metadata):
        dummy = Dummy()
        dummy._extract_metadata({Metadata.KEY_METADATA: metadata})
        string_repr = dummy.get_metadata_string()
        assert ast.literal_eval(string_repr) == metadata

    def test__has_metadata__no_metadata__returns_false(self):
        dummy = Dummy()
        assert not dummy.has_metadata()

    def test__has_metadata__metadata_present__returns_true(self):
        dummy = Dummy()
        dummy._extract_metadata({Metadata.KEY_METADATA: {"Some": "MetaData"}})
        assert dummy.has_metadata()

    def test__get_metadata_string__no_metadata__returns_empty(self):
        dummy = Dummy()
        assert dummy.get_metadata_string() is ""

    def test__to_dict__no_child_data_no_metadata__returns_empty_dict(self):
        dummy = Dummy(definitions={}, my_data={})
        assert dummy.to_dict() == {}

    def test__to_dict__child_data_no_metadata__returns_child_data(self):
        child_data = {"A": "B", "C": "D"}
        dummy = Dummy(definitions={}, my_data=child_data)
        assert dummy.to_dict() == child_data

    @pytest.mark.parametrize("metadata", metadata_test_variations)
    def test__to_dict__child_data_and_metadata__keeps_child_data(self, metadata):
        child_data = {"A": "B", "C": "D"}
        dummy = Dummy(definitions={Dummy.KEY_METADATA: metadata}, my_data=child_data)
        result = dummy.to_dict()
        for key, value in child_data.items():
            assert result[key] == value

    @pytest.mark.parametrize("metadata", metadata_test_variations)
    def test__to_dict__child_data_and_metadata__adds_meta_data(self, metadata):
        child_data = {"A": "B", "C": "D"}
        dummy = Dummy(definitions={Dummy.KEY_METADATA: metadata}, my_data=child_data)
        result = dummy.to_dict()[Dummy.KEY_METADATA]
        for key, value in metadata.items():
            assert result[key] == value


class TestMetadataComponent:
    def test__init__none__passes(self):
        spec = MetadataComponent(None)
        assert spec.to_dict() == {}

    def test__init__empty__passes(self):
        spec = MetadataComponent({})
        assert spec.to_dict() == {}

    def test__init__with_metadata__extracted(self):
        definition = {MetadataComponent.KEY_METADATA: {"A": "B"}}
        spec = MetadataComponent(copy.deepcopy(definition))
        assert spec.to_dict() == definition


class TestValueContainer:
    """Tests for ValueContainer class"""

    def test__init__none__sets_empty_values(self):
        container = ValueContainer()
        assert container.values == {}

    def test__init__empty_dict__sets_empty_values(self):
        container = ValueContainer({})
        assert container.values == {}

    def test__init__empty_list__sets_empty_values(self):
        container = ValueContainer([])
        assert container.values == {}

    def test__init__illegal__raises(self):
        with pytest.raises(InputError) as e_info:
            # noinspection PyTypeChecker
            ValueContainer("This is not a valid input")
        assert_exception_contains(ValueContainer._ERR_VALUES_ILL_FORMATTED, e_info)

    @pytest.mark.parametrize("definition", [[], ["A"], ["A", "B"], [1, "A", 4.2]])
    def test__init__list__sets_keys(self, definition: list):
        container = ValueContainer(definition)
        assert container.as_list() == definition

    @pytest.mark.parametrize("definition", [{}, {"A": {}}, {"A": {}, "B": {}}, {1: {}, "A": {}, 4.2: {}}])
    def test__init__dict__sets_keys(self, definition: dict):
        container = ValueContainer(definition)
        assert container.as_list() == list(definition.keys())

    def test__init__dict_with_metadata__metadata_assigned(self):
        definition = {"A": {Metadata.KEY_METADATA: {"Some": "Metadata"}}, "B": {}}
        container = ValueContainer(copy.deepcopy(definition))
        assert container.to_dict() == definition

    def test__has_value__value_present_returns_true(self):
        container = ValueContainer(["A", "B", "C"])
        assert container.has_value("B")

    def test__has_value__value_missing_returns_false(self):
        container = ValueContainer(["A", "B", "C"])
        assert not container.has_value("X")

    def test__is_empty__empty__returns_true(self):
        container = ValueContainer()
        assert container.is_empty()

    def test__is_empty__not_empty__returns_false(self):
        container = ValueContainer(["A", "B", "C"])
        assert not container.is_empty()
