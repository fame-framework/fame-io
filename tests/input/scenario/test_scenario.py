# SPDX-FileCopyrightText: 2023 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
import copy

import pytest

from fameio.input import ScenarioError
from fameio.input.scenario import Scenario, Agent, Contract
from tests.utils import assert_exception_contains, new_agent, new_schema, new_string_set


def new_scenario(
    schema: dict = None,
    general: dict = None,
    agents: list = None,
    contracts: list = None,
    string_sets: dict = None,
    metadata: dict = None,
) -> Scenario:
    """Creates a new scenario from the given components"""
    scenario = {}
    if schema:
        scenario[Scenario.KEY_SCHEMA] = schema
    if general:
        scenario[Scenario.KEY_GENERAL] = general
    if agents:
        scenario[Scenario.KEY_AGENTS] = agents
    if contracts:
        scenario[Scenario.KEY_CONTRACTS] = contracts
    if string_sets:
        scenario[Scenario.KEY_STRING_SETS] = string_sets
    if metadata:
        scenario[Scenario.KEY_METADATA] = metadata
    return Scenario.from_dict(scenario)


class TestScenario:
    _schema = new_schema([new_agent("MyAgent", [], [])])
    _general = {
        "runid": 1,
        "simulation": {"starttime": 0, "stoptime": 0, "randomseed": 0},
    }
    _metadata = {"Some": "Metadata", "Are": "specified", "here": 5}

    def test__init__missing_schema__raises(self):
        with pytest.raises(ScenarioError) as e_info:
            new_scenario(None, self._general, [], [])
        assert_exception_contains(Scenario._MISSING_KEY, e_info)

    def test__init__missing_general_properties__raises(self):
        with pytest.raises(ScenarioError) as e_info:
            new_scenario(self._schema, None, [], [])
        assert_exception_contains(Scenario._MISSING_KEY, e_info)

    def test__init__no_agent__accept(self):
        new_scenario(self._schema, self._general, None, [])

    def test__init__no_contract__accept(self):
        new_scenario(self._schema, self._general, [], None)

    def test__general_properties(self):
        scenario = new_scenario(self._schema, self._general, None, [])
        assert scenario.general_properties.to_dict() == self._general

    def test__schema(self):
        scenario = new_scenario(self._schema, self._general, None, [])
        assert scenario.schema.to_dict() == self._schema

    def test__from_dict__to_dict__minimal(self):
        original = {Scenario.KEY_SCHEMA: self._schema, Scenario.KEY_GENERAL: self._general}
        result = Scenario.from_dict(original).to_dict()
        assert result == original

    def test__from_dict__to_dict__agents(self):
        original = {
            Scenario.KEY_SCHEMA: self._schema,
            Scenario.KEY_GENERAL: self._general,
            Scenario.KEY_AGENTS: [{Agent.KEY_TYPE: "MyAgent", Agent.KEY_ID: 1}],
        }
        result = Scenario.from_dict(original).to_dict()
        assert result == original

    def test__from_dict__to_dict__contracts(self):
        original = {
            Scenario.KEY_SCHEMA: self._schema,
            Scenario.KEY_GENERAL: self._general,
            Scenario.KEY_CONTRACTS: [
                {
                    Contract.KEY_SENDER: 1,
                    Contract.KEY_RECEIVER: 2,
                    Contract.KEY_PRODUCT: "SomeProduct",
                    Contract.KEY_FIRST_DELIVERY: 0,
                    Contract.KEY_INTERVAL: 100,
                }
            ],
        }
        result = Scenario.from_dict(original).to_dict()
        assert result == original

    def test__from__to_dict__string_sets(self):
        original = {
            Scenario.KEY_SCHEMA: self._schema,
            Scenario.KEY_GENERAL: self._general,
            Scenario.KEY_STRING_SETS: {
                "FUEL_TYPE": {
                    "values": {
                        "OIL": {},
                        "LIGNITE": {
                            "metadata": {
                                "Origin": "Study",
                                "Date": "Today",
                            }
                        },
                    },
                }
            },
        }
        result = Scenario.from_dict(copy.deepcopy(original)).to_dict()
        assert result == original

    def test__from_dict__sets_metadata(self):
        scenario = new_scenario(self._schema, self._general, None, [], None, self._metadata)
        assert scenario.metadata == self._metadata

    def test__add_string_set__added(self):
        scenario = new_scenario(self._schema, self._general, None, [])
        scenario.add_string_set("NewStringSet", new_string_set(["A", "B", "C"]))
        assert "NewStringSet" in scenario.string_sets

    def test__add_string_set_add_two__both_added(self):
        scenario = new_scenario(self._schema, self._general, None, [])
        scenario.add_string_set("NewStringSet", new_string_set(["A", "B", "C"]))
        scenario.add_string_set("OtherStringSet", new_string_set(["X", "Y", "Z"]))
        assert "NewStringSet" in scenario.string_sets
        assert "OtherStringSet" in scenario.string_sets
