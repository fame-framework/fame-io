# SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
import copy

import pytest

from fameio.input import ScenarioError
from fameio.input.scenario import StringSet
from tests.log_utils import fame_log


class TestStringSet:
    def test__from_dict__empty_definition__raises(self, fame_log):
        with pytest.raises(ScenarioError) as e_info:
            StringSet.from_dict({})
        fame_log.assert_logged_exception(StringSet._ERR_KEY_MISSING, e_info)

    def test__from_dict__value_list__is_in_set(self):
        string_set = StringSet.from_dict({StringSet.KEY_VALUES: ["Paper", "Cardboard"]})
        assert string_set.is_in_set("Paper")
        assert string_set.is_in_set("Cardboard")

    def test__from_dict__value_dict__is_in_set(self):
        definition = {
            StringSet.KEY_VALUES: {
                "A": {"B": 1},
                "C": {},
            }
        }
        string_set = StringSet.from_dict(definition)
        assert string_set.is_in_set("A")
        assert string_set.is_in_set("C")

    @pytest.mark.parametrize("key", ["Values", "values", "VALUES", "vAlUeS"])
    def test__from_dict__allowed_keys(self, key):
        definition = {
            key: {
                "A": {"B": 1},
                "C": {},
            }
        }
        string_set = StringSet.from_dict(definition)
        assert string_set.is_in_set("A")
        assert string_set.is_in_set("C")

    def test__from_dict__sets_metadata(self):
        definition = {
            StringSet.KEY_VALUES: {
                "A": {"B": 1},
                "C": {},
            },
            StringSet.KEY_METADATA: {
                "X": "Y",
            },
        }
        string_set = StringSet.from_dict(definition)
        assert string_set.metadata == {"X": "Y"}

    def test__from_dict__to_dict__equal(self):
        definition = {
            StringSet.KEY_VALUES: {
                "A": {StringSet.KEY_METADATA: {"A": 1}},
                "C": {},
            },
            StringSet.KEY_METADATA: {
                "X": "Y",
            },
        }
        string_set = StringSet.from_dict(copy.deepcopy(definition))
        assert string_set.to_dict() == definition

    def test__is_in_set__not_in_set__returns_false(self):
        definition = {
            StringSet.KEY_VALUES: {
                "A": {"B": 1},
                "C": {},
            },
        }
        string_set = StringSet.from_dict(definition)
        assert string_set.is_in_set("X") is False

    def test__values__no_values__returns_empty_dict(self):
        assert StringSet().values == {}

    def test__values__values_present__returns__values(self):
        values = {
            "A": {},
            "B": {},
            "C": {},
        }
        definition = {StringSet.KEY_VALUES: values}
        assert StringSet.from_dict(definition).values.keys() == values.keys()
