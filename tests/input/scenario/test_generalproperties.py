# SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
import logging

import pytest

from fameio.input import ScenarioError
from fameio.input.scenario import GeneralProperties
from tests.log_utils import fame_log
from tests.utils import assert_exception_contains


class TestGeneralProperties:
    def test__init__run_id_missing__set_to_one(self):
        general = GeneralProperties.from_dict({"Simulation": {"StartTime": 0, "StopTime": 0}})
        assert general.run_id == 1

    def test__init__run_id__set(self):
        run_id = 66
        general = GeneralProperties.from_dict({"RunId": run_id, "Simulation": {"StartTime": 0, "StopTime": 0}})
        assert general.run_id == run_id

    def test__init__simulation_missing__raises(self):
        with pytest.raises(ScenarioError) as e_info:
            GeneralProperties.from_dict({"RunId": 66})
        assert_exception_contains(GeneralProperties._ERR_MISSING_KEY, e_info)

    def test__init__start_time_missing__raises(self):
        with pytest.raises(ScenarioError) as e_info:
            GeneralProperties.from_dict({"RunId": 66, "Simulation": {"StopTime": 0}})
        assert_exception_contains(GeneralProperties._ERR_MISSING_KEY, e_info)

    def test__init__stop_time_missing__raises(self):
        with pytest.raises(ScenarioError) as e_info:
            GeneralProperties.from_dict({"RunId": 66, "Simulation": {"StartTime": 0}})
        assert_exception_contains(GeneralProperties._ERR_MISSING_KEY, e_info)

    def test__init__negative_simulation_duration__logs_warning(self, fame_log):
        fame_log.set_level(logging.WARNING)
        GeneralProperties.from_dict({"Simulation": {"StartTime": 1, "StopTime": 0}})
        fame_log.assert_logged(GeneralProperties._ERR_SIMULATION_DURATION)

    def test__init__random_seed_missing__set_to_one(self):
        general = GeneralProperties.from_dict({"Simulation": {"StartTime": 0, "StopTime": 0}})
        assert general.simulation_random_seed == 1

    def test__init__random_seed__set(self):
        seed = 87
        general = GeneralProperties.from_dict({"Simulation": {"StartTime": 0, "StopTime": 0, "RandomSeed": seed}})
        assert general.simulation_random_seed == seed
