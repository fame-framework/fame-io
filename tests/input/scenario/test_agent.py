# SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
import logging
from typing import Optional

import pytest
from mockito import mock

from fameio.input import ScenarioError
from fameio.input.scenario import Agent, Attribute
from tests.log_utils import fame_log
from tests.utils import assert_exception_contains


class CustomizedAgent(Agent):
    def __init__(self, agent_id: int, type_name: str) -> None:
        super().__init__(agent_id, type_name)
        self._notified_count = 0

    def _notify_data_changed(self):
        self._notified_count += 1

    @property
    def notified_count(self) -> int:
        return self._notified_count


class TestAgentWithNotification:
    def test_add_attribute__notifies(self):
        a = CustomizedAgent(agent_id=42, type_name="AgentType")
        a.add_attribute("attr_name", Attribute("attr_full_name", 10))
        assert a.notified_count == 1


class TestAgent:
    @pytest.fixture
    def agent(self):
        return Agent(agent_id=42, type_name="AgentType")

    @staticmethod
    def new_agent(
        agent_id: Optional[int], type_name: Optional[str], attributes: Optional[dict], metadata: Optional[dict]
    ) -> dict:
        """Returns a dictionary representing the agent with given parameters"""
        agent = {Agent.KEY_ID: agent_id, Agent.KEY_TYPE: type_name}
        if attributes:
            agent[Agent.KEY_ATTRIBUTES] = attributes
        if metadata:
            agent[Agent.KEY_METADATA] = metadata
        return agent

    def test_init__missing_id__raises(self):
        with pytest.raises(ScenarioError) as e_info:
            # noinspection PyTypeChecker
            Agent(agent_id=None, type_name="AgentType")
        assert_exception_contains(Agent._ERR_MISSING_ID, e_info)

    def test_init__negative_id__raises(self):
        with pytest.raises(ScenarioError) as e_info:
            Agent(agent_id=-42, type_name="AgentType")
        assert_exception_contains(Agent._ERR_MISSING_ID, e_info)

    def test_init__missing_type__raises(self):
        with pytest.raises(ScenarioError) as e_info:
            # noinspection PyTypeChecker
            Agent(agent_id=42, type_name=None)
        assert_exception_contains(Agent._ERR_MISSING_TYPE, e_info)

    def test_init__empty_type__raises(self):
        with pytest.raises(ScenarioError) as e_info:
            Agent(agent_id=42, type_name=" ")
        assert_exception_contains(Agent._ERR_MISSING_TYPE, e_info)

    def test_init__id_is_set(self, agent):
        a = Agent(agent_id=42, type_name="AgentType")
        assert a.id == 42
        assert a.display_id == "#42"

    def test_init__type_is_set(self):
        a = Agent(agent_id=42, type_name="AgentType")
        assert a.type_name == "AgentType"

    def test_init__has_empty_attributes(self):
        a = Agent(agent_id=42, type_name="AgentType")
        assert a.attributes == {}

    def test_init__has_empty_metadata(self):
        a = Agent(agent_id=42, type_name="AgentType")
        assert a.metadata == {}

    def test_add_attribute__added(self, agent):
        attribute = mock(Attribute)
        agent.add_attribute("myAttrib", attribute)
        assert agent.attributes["myAttrib"] == attribute

    def test_add_attribute__already_exists_raises(self, agent):
        attribute = mock(Attribute)
        agent.add_attribute("myAttrib", attribute)
        with pytest.raises(ValueError) as e_info:
            agent.add_attribute("myAttrib", attribute)
        assert_exception_contains(Agent._ERR_DOUBLE_ATTRIBUTE, e_info)

    def test_from_dict__type_missing__raises(self, fame_log):
        fame_log.set_level(logging.ERROR)
        with pytest.raises(ScenarioError) as e_info:
            Agent.from_dict(self.new_agent(42, None, None, None))
        fame_log.assert_logged_exception(Agent._ERR_MISSING_TYPE, e_info)

    def test_from_dict__id_missing__raises(self, fame_log):
        fame_log.set_level(logging.ERROR)
        with pytest.raises(ScenarioError) as e_info:
            Agent.from_dict(self.new_agent(None, "MyAgent", None, None))
        fame_log.assert_logged_exception(Agent._ERR_MISSING_ID, e_info)

    def test_from_dict__no_attributes__returns_agent(self):
        agent = Agent.from_dict(self.new_agent(42, "MyAgent", None, None))
        assert isinstance(agent, Agent)

    def test_from_dict__attributes__added_to_agent(self):
        attributes = {"a": 2, "b": 9.2, "c": "d"}
        agent = Agent.from_dict(self.new_agent(42, "MyAgent", attributes, None))
        for attribute, value in attributes.items():
            assert attribute in agent.attributes
            assert agent.attributes[attribute].value == value

    def test_from_dict__metadata__added_to_agent(self):
        metadata = {"a": 2, "b": 9.2, "c": "d"}
        agent = Agent.from_dict(self.new_agent(42, "MyAgent", None, metadata))
        assert agent.metadata == metadata

    def test_init__attributes__on_overwrite__raises(self, fame_log):
        attributes = {"a": 2, "b": 9.2, "c": "d"}
        agent = Agent.from_dict(self.new_agent(42, "MyAgent", attributes, None))
        fame_log.set_level(logging.ERROR)
        with pytest.raises(ScenarioError) as e_info:
            agent.init_attributes_from_dict(attributes)
        fame_log.assert_logged_exception(Agent._ERR_ATTRIBUTE_OVERWRITE, e_info)

    def test_to_dict__contains_id(self):
        result = Agent(agent_id=42, type_name="AgentType").to_dict()
        assert result[Agent.KEY_ID] == 42

    def test_to_dict__contains_type(self):
        result = Agent(agent_id=42, type_name="AgentType").to_dict()
        assert result[Agent.KEY_TYPE] == "AgentType"

    def test_to_dict__missing_attributes__not_contained(self):
        assert Agent.KEY_ATTRIBUTES not in Agent(agent_id=42, type_name="AgentType").to_dict()

    def test_to_dict__missing_metadata__not_contained(self):
        assert Agent.KEY_METADATA not in Agent(agent_id=42, type_name="AgentType").to_dict()

    def test_to_dict__attributes__contained(self):
        attributes = {"a": 2, "b": 9.2, "c": "d"}
        result = Agent.from_dict(self.new_agent(42, "MyAgent", attributes, None)).to_dict()
        assert Agent.KEY_ATTRIBUTES in result
        for attribute, value in attributes.items():
            assert result[Agent.KEY_ATTRIBUTES][attribute] == {Attribute.KEY_VALUE: value}

    def test_to_dict__metadata__contained(self):
        metadata = {"a": 2, "b": 9.2, "c": "d"}
        result = Agent.from_dict(self.new_agent(42, "MyAgent", None, metadata)).to_dict()
        assert Agent.KEY_METADATA in result
        assert result[Agent.KEY_METADATA] == metadata

    def test_from_string__of_to_string__returns_original_dict(self):
        dict_original = self.new_agent(
            agent_id=42,
            type_name="MyAgent",
            attributes={"x": {Attribute.KEY_VALUE: 2}, "y": {Attribute.KEY_VALUE: "lala"}},
            metadata={"a": 2, "b": 9.2, "c": "d"},
        )
        agent_string = Agent.from_dict(dict_original).to_string()
        agent = Agent.from_string(agent_string)
        assert agent.to_dict() == dict_original
