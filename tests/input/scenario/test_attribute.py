# SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
import copy

import pytest

from fameio.input import ScenarioError
from fameio.input.scenario import Attribute
from tests.utils import assert_exception_contains


class TestAttribute:
    _SAMPLE_METADATA = {"Some": "Metadata"}

    def test__init__none__raises(self):
        with pytest.raises(ScenarioError) as e_info:
            # noinspection PyTypeChecker
            Attribute(name="AnAttribute", definitions=None)
        assert_exception_contains(Attribute._ERR_VALUE_MISSING, e_info)

    def test__init__name___is_representation(self):
        name = "AnAttribute"
        attribute = Attribute(name=name, definitions="AnyValue")
        assert name == str(attribute)

    @pytest.mark.parametrize("value", [-10, 0, 5, 0.1, -5.0, 4.2, "", "SomeString"])
    def test__init__single_value_no_meta__assigned(self, value):
        attribute = Attribute(name="AnyAttributeName", definitions=value)
        assert attribute.value == value

    @pytest.mark.parametrize("value", [-10, 0, 5, 0.1, -5.0, 4.2, "", "SomeString"])
    def test__init__to_dict__single_value_no_meta__dict_correct(self, value):
        attribute = Attribute(name="AnyAttributeName", definitions=value)
        assert attribute.to_dict() == {Attribute.KEY_VALUE: value}

    @pytest.mark.parametrize("value", [-10, 0, 5, 0.1, -5.0, 4.2, "", "SomeString"])
    def test__init__single_value_with_meta__value_assigned(self, value):
        definitions = {Attribute.KEY_VALUE: value, Attribute.KEY_METADATA: self._SAMPLE_METADATA}
        attribute = Attribute(name="AnyAttributeName", definitions=definitions)
        assert attribute.value == value

    @pytest.mark.parametrize("value", [-10, 0, 5, 0.1, -5.0, 4.2, "", "SomeString"])
    def test__init__to_dict__single_value_with_meta__dict_correct(self, value):
        definitions = {Attribute.KEY_VALUE: value, Attribute.KEY_METADATA: self._SAMPLE_METADATA}
        attribute = Attribute(name="AnyAttributeName", definitions=copy.deepcopy(definitions))
        assert attribute.to_dict() == definitions

    @pytest.mark.parametrize("values", [[0], [0, 1, 2], ["SomeString"], ["Some", "Strings"], [1.2], [4.2, 2.4, -1.0]])
    def test__init__value_list_no_meta__assigned(self, values):
        attribute = Attribute(name="AnyAttributeName", definitions=values)
        assert attribute.value == values

    def test__init__value_list_empty__raises(self):
        with pytest.raises(ScenarioError) as e_info:
            Attribute("AnyAttributeName", [])
        assert_exception_contains(Attribute._ERR_LIST_EMPTY, e_info)

    @pytest.mark.parametrize("values", [[0], [0, 1, 2], ["SomeString"], ["Some", "Strings"], [1.2], [4.2, 2.4, -1.0]])
    def test__init__to_dict__value_list_no_meta__dict_correct(self, values):
        attribute = Attribute(name="AnyAttributeName", definitions=values)
        assert attribute.to_dict() == {Attribute.KEY_VALUES: [{Attribute.KEY_VALUE: value} for value in values]}

    @pytest.mark.parametrize("values", [[0], [0, 1, 2], ["SomeString"], ["Some", "Strings"], [1.2], [4.2, 2.4, -1.0]])
    def test__init__value_list_with_general_meta__assigned(self, values):
        definitions = {Attribute.KEY_VALUES: values, Attribute.KEY_METADATA: self._SAMPLE_METADATA}
        attribute = Attribute(name="AnyAttributeName", definitions=definitions)
        assert attribute.value == values

    def test__init__to_dict__value_list_with_general_metadata__metadata_assigned(self):
        definitions = {Attribute.KEY_VALUES: [0, 1, 2], Attribute.KEY_METADATA: self._SAMPLE_METADATA}
        attribute = Attribute(name="AnyAttributeName", definitions=definitions)
        assert attribute.to_dict().get(Attribute.KEY_METADATA) == self._SAMPLE_METADATA

    def test__init__to_dict__value_list_with_detail_metadata__returns_correct_dict(self):
        definitions = {
            Attribute.KEY_VALUES: [
                {Attribute.KEY_VALUE: 0, Attribute.KEY_METADATA: self._SAMPLE_METADATA},
                {Attribute.KEY_VALUE: 1, Attribute.KEY_METADATA: self._SAMPLE_METADATA},
                {Attribute.KEY_VALUE: 2, Attribute.KEY_METADATA: self._SAMPLE_METADATA},
            ]
        }
        attribute = Attribute(name="AnyAttributeName", definitions=copy.deepcopy(definitions))
        assert attribute.to_dict() == definitions

    def test__init__value_list_mixed_data_and_metadata__assigned(self):
        values = [1, {Attribute.KEY_VALUE: 2}, {Attribute.KEY_VALUE: 3, Attribute.KEY_METADATA: self._SAMPLE_METADATA}]
        attribute = Attribute(name="AnyAttributeName", definitions=values)
        assert attribute.value == [1, 2, 3]

    def test__init__empty_dict__raises(self):
        with pytest.raises(ScenarioError) as e_info:
            Attribute("AnyAttributeName", {})
        assert_exception_contains(Attribute._ERR_DICT_EMPTY, e_info)

    def test__init__nested_no_meta__assigns_inner_attributes(self):
        attribute = Attribute(name="AnyAttributeName", definitions={"A": "X", "B": 1, "C": 4.2})
        for key in ["A", "B", "C"]:
            assert isinstance(attribute.nested.get(key), Attribute)

    def test__init__nested_no_meta__inner_names_extend_parent(self):
        parent_name = "ParentAttribute"
        attribute = Attribute(name=parent_name, definitions={"A": "X", "B": 1, "C": 4.2})
        for key in ["A", "B", "C"]:
            assert str(attribute.nested.get(key)) == parent_name + Attribute.NAME_STRING_SEPARATOR + key

    def test__init__to_dict__nested_general_meta__contains_metadata(self):
        definitions = {"A": "X", "B": 1, "C": 4.2, Attribute.KEY_METADATA: self._SAMPLE_METADATA}
        attribute = Attribute(name="AnyAttributeName", definitions=definitions)
        assert attribute.to_dict().get(Attribute.KEY_METADATA) == self._SAMPLE_METADATA

    def test__init__nested_list_no_meta__assigns_list_of_inner_attribute_dicts(self):
        definitions = [{"A": "X", "B": 1, "C": 4.2}, {"A": "Y", "B": 2, "C": 42.0}]
        attribute = Attribute(name="AnyAttributeName", definitions=definitions)
        for entry in attribute.nested_list:
            for key in ["A", "B", "C"]:
                assert key in entry.keys()

    def test__init__nested_list_no_meta__inner_names_extend_parent_with_list_index(self):
        definitions = [{"A": "X", "B": 1, "C": 4.2}, {"A": "Y", "B": 2, "C": 42.0}]
        parent_name = "ParentAttribute"
        attribute = Attribute(name=parent_name, definitions=definitions)
        for index, entry in enumerate(attribute.nested_list):
            for key in ["A", "B", "C"]:
                expected_name = (
                    parent_name + Attribute.NAME_STRING_SEPARATOR + str(index) + Attribute.NAME_STRING_SEPARATOR + key
                )
                assert str(entry.get(key)) == expected_name

    def test__init__nested_list_no_meta__inner_values_assigned(self):
        attribute = Attribute("AnAttribute", [{"A": 1, "B": 2}, {"A": 10, "B": 20}])
        assert attribute.has_nested_list
        assert not attribute.has_nested
        assert not attribute.has_value
        nested_list = attribute.nested_list
        assert len(nested_list) == 2
        assert nested_list[0]["A"].value == 1
        assert nested_list[1]["A"].value == 10

    def test__init__to_dict__nested_list_general_meta_metadata_in_dict(self):
        definitions = {
            Attribute.KEY_VALUES: [{"A": "X", "B": 1, "C": 4.2}, {"A": "Y", "B": 2, "C": 42.0}],
            Attribute.KEY_METADATA: self._SAMPLE_METADATA,
        }
        attribute = Attribute(name="AnyAttributeName", definitions=definitions)
        assert attribute.to_dict().get(Attribute.KEY_METADATA) == self._SAMPLE_METADATA

    def test__init__to_dict__nested_list_with_detail_metadata__assigns_entry_metadata(self):
        definitions = [
            {"A": "X", "B": 1, "C": 4.2, Attribute.KEY_METADATA: self._SAMPLE_METADATA},
            {"A": "Y", "B": 2, "C": 42.0, Attribute.KEY_METADATA: self._SAMPLE_METADATA},
        ]
        attribute = Attribute(name="AnyAttributeName", definitions=definitions)
        for entry in attribute.to_dict()[Attribute.KEY_VALUES]:
            assert entry.get(Attribute.KEY_METADATA) == self._SAMPLE_METADATA

    def test__init__inconsistent_data_in_list__raises(self):
        with pytest.raises(ScenarioError) as e_info:
            Attribute("AnAttribute", [{"A": 1, "B": 2}, 5])
        assert_exception_contains(Attribute._ERR_MIXED_DATA, e_info)

    def test__init__inconsistent_data_in_values__raises(self):
        with pytest.raises(ScenarioError) as e_info:
            Attribute("AnAttribute", {Attribute.KEY_VALUES: [5, {"A": "B"}]})
        assert_exception_contains(Attribute._ERR_MIXED_DATA, e_info)

    def test__has_value__single_value__returns_true(self):
        attribute = Attribute(name="AnyAttributeName", definitions="SomeString")
        assert attribute.has_value

    def test__has_value__value_list__returns_true(self):
        attribute = Attribute(name="AnyAttributeName", definitions=["SomeString"])
        assert attribute.has_value

    def test__has_value__nested__returns_false(self):
        attribute = Attribute(name="AnyAttributeName", definitions={"An": "InnerValue"})
        assert not attribute.has_value

    def test__has_value__nested_list__returns_false(self):
        attribute = Attribute(name="AnyAttributeName", definitions=[{"An": "InnerValue"}])
        assert not attribute.has_value

    def test__has_nested__single_value__returns_false(self):
        attribute = Attribute(name="AnyAttributeName", definitions="SomeString")
        assert not attribute.has_nested

    def test__has_nested__value_list__returns_false(self):
        attribute = Attribute(name="AnyAttributeName", definitions=["SomeString"])
        assert not attribute.has_nested

    def test__has_nested__nested__returns_true(self):
        attribute = Attribute(name="AnyAttributeName", definitions={"An": "InnerValue"})
        assert attribute.has_nested

    def test__has_nested__nested_list__returns_false(self):
        attribute = Attribute(name="AnyAttributeName", definitions=[{"An": "InnerValue"}])
        assert not attribute.has_nested

    def test__has_nested_list__single_value__returns_false(self):
        attribute = Attribute(name="AnyAttributeName", definitions="SomeString")
        assert not attribute.has_nested_list

    def test__has_nested_list__value_list__returns_false(self):
        attribute = Attribute(name="AnyAttributeName", definitions=["SomeString"])
        assert not attribute.has_nested_list

    def test__has_nested_list__nested__returns_false(self):
        attribute = Attribute(name="AnyAttributeName", definitions={"An": "InnerValue"})
        assert not attribute.has_nested_list

    def test__has_nested_list__nested_list__returns_true(self):
        attribute = Attribute(name="AnyAttributeName", definitions=[{"An": "InnerValue"}])
        assert attribute.has_nested_list

    def test_init_nested_fail_missing_value(self):
        with pytest.raises(ScenarioError) as e_info:
            Attribute("AnAttribute", {"InnerAttrib": None})
        assert_exception_contains(Attribute._ERR_VALUE_MISSING, e_info)

    def test__value__no_single_value__returns_none(self):
        attribute = Attribute(name="AnyAttributeName", definitions={"An": "InnerValue"})
        assert attribute.value is None

    def test__nested__no_nested_value__returns_none(self):
        attribute = Attribute(name="AnyAttributeName", definitions="SomeString")
        assert attribute.nested is None

    def test__nested_list__no_nested_value__returns_none(self):
        attribute = Attribute(name="AnyAttributeName", definitions="SomeString")
        assert attribute.nested_list is None
