# SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
from os import path

from fameio.input.resolver import PathResolver
from tests.mock_utils import change_dir


class TestPathResolver:
    _THIS_FILE = "test_resolver.py"

    def test__search_file_in_directory__file_in_directory__returns_relative_path(self, change_dir):
        result = PathResolver._search_file_in_directory(file_name=self._THIS_FILE, directory=path.curdir)
        assert result == path.join(path.curdir, self._THIS_FILE)

    def test__search_file_in_directory__file_not_in_directory__returns_none(self, change_dir):
        result = PathResolver._search_file_in_directory(file_name="ThisFileDoesNot.exist", directory=path.curdir)
        assert result is None

    def test__resolve_series_file_path__is_abs_path__returns_input(self, change_dir):
        absolute_path = path.abspath(path.join(path.curdir, self._THIS_FILE))
        result = PathResolver().resolve_series_file_path(file_name=absolute_path)
        assert result == absolute_path

    def test__resolve_series_file_path__file_exists_in_cwd__returns_path_to_file(self, change_dir):
        result = PathResolver().resolve_series_file_path(file_name=self._THIS_FILE)
        assert result == path.join(path.curdir, self._THIS_FILE)

    def test__resolve_series_file_path__file_not_in_cwd__returns_none(self, change_dir):
        result = PathResolver().resolve_series_file_path(file_name="ThisFileDoesNot.exist")
        assert result is None

    def test__resolve_file_pattern__directory_missing__returns_empty_list(self, change_dir):
        result = PathResolver().resolve_file_pattern("./thisDirectoryIsMissing", "*.yaml")
        assert isinstance(result, list)
        assert len(result) == 0

    def test__resolve_file_pattern__no_pattern_match__returns_empty_list(self, change_dir):
        result = PathResolver().resolve_file_pattern("..", "*.noValidPattern")
        assert isinstance(result, list)
        assert len(result) == 0

    def test__resolve_file_pattern__valid_pattern_and_directory__returns_file_list(self, change_dir):
        result = PathResolver().resolve_file_pattern("..", "*.py")
        assert isinstance(result, list)
        assert len(result) > 0
