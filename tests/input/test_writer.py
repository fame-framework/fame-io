# SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
import builtins
import importlib.metadata
import logging
from io import TextIOWrapper
from pathlib import Path
from typing import Union

import pytest
from fameprotobuf.contract_pb2 import ProtoContract
from fameprotobuf.data_storage_pb2 import DataStorage
from fameprotobuf.execution_data_pb2 import ExecutionData
from fameprotobuf.field_pb2 import NestedField
from fameprotobuf.input_file_pb2 import InputData
from fameprotobuf.model_pb2 import ModelData
from google.protobuf.json_format import MessageToDict
from mockito import mock, when, any, verify, patch
from mockito.mocking import Mock

from fameio.input.scenario import GeneralProperties, Agent, Attribute, Contract, Scenario, StringSet
from fameio.input.schema import Schema, AgentType, JavaPackages
from fameio.input.writer import ProtoWriter, ProtoWriterError
from fameio.series import TimeSeriesManager
from tests.input.scenario.test_agent import TestAgent
from tests.input.scenario.test_contract import TestContract
from tests.log_utils import fame_log
from tests.mock_utils import un_stub, add_context_manager


@pytest.fixture
def stub_writing() -> Mock:
    io_mock = add_context_manager(mock(TextIOWrapper))
    when(builtins).open(Path(), any(str)).thenReturn(io_mock)
    when(io_mock).write(any())
    return io_mock


class TestProtoWriter:
    _schema = Schema.from_dict(
        {
            Schema.KEY_AGENT_TYPE: {
                "MyAgent": {
                    AgentType.KEY_ATTRIBUTES: {
                        "Int": {"AttributeType": "integer", "Mandatory": False, "List": False},
                        "Double": {"AttributeType": "double", "Mandatory": False, "List": False},
                        "DoubleList": {"AttributeType": "double", "Mandatory": False, "List": True},
                        "Enum": {"AttributeType": "enum", "Mandatory": False, "List": False},
                        "StringSet": {"AttributeType": "string_set", "Mandatory": False, "List": False},
                        "Long": {"AttributeType": "long", "Mandatory": False, "List": False},
                        "Series": {"AttributeType": "time_series", "Mandatory": False, "List": False},
                        "Block": {
                            "AttributeType": "block",
                            "Mandatory": False,
                            "List": False,
                            "NestedAttributes": {
                                "InnerInt": {"AttributeType": "integer", "Mandatory": False, "List": False},
                            },
                        },
                        "BlockList": {
                            "AttributeType": "block",
                            "Mandatory": False,
                            "List": True,
                            "NestedAttributes": {
                                "InnerDouble": {"AttributeType": "double", "Mandatory": False, "List": False},
                            },
                        },
                    }
                }
            },
            Schema.KEY_PACKAGES: {
                JavaPackages.KEY_AGENT: ["myAgentPackage1", "myAgentPackage2"],
                JavaPackages.KEY_DATA_ITEM: ["myDataItemPackage1", "myDataItemPackage2"],
                JavaPackages.KEY_PORTABLE: ["myPortablePackage1", "myPortablePackage2"],
            },
        }
    )

    def test__set_general_properties__all_properties__all_set(self):
        pb_input = self._initialize_pb_input()
        general_properties = self._initialize_general_properties()
        ProtoWriter._set_general_properties(pb_input, general_properties)
        assert MessageToDict(pb_input)["runId"] == str(general_properties.run_id)
        assert MessageToDict(pb_input)["simulation"]["startTime"] == str(general_properties.simulation_start_time)
        assert MessageToDict(pb_input)["simulation"]["stopTime"] == str(general_properties.simulation_stop_time)
        assert MessageToDict(pb_input)["simulation"]["randomSeed"] == str(general_properties.simulation_random_seed)

    @staticmethod
    def _initialize_pb_input() -> InputData:
        """Returns new empty `InputData` of a new DataStorage"""
        pb_data_storage = DataStorage()
        return pb_data_storage.input

    @staticmethod
    def _initialize_general_properties() -> GeneralProperties:
        """Returns new complete dummy GeneralProperties"""
        properties = {
            GeneralProperties.KEY_RUN: 42,
            GeneralProperties.KEY_SIMULATION: {
                GeneralProperties.KEY_START: 0,
                GeneralProperties.KEY_STOP: 100,
                GeneralProperties.KEY_SEED: 123,
            },
        }
        general_properties = GeneralProperties.from_dict(properties)
        return general_properties

    def test__add_agents__one_agent_no_attributes_no_metadata__added(self):
        pb_input = self._initialize_pb_input()
        agent = self._create_agent(agent_id=42, type_name="MyAgent", attributes=None, metadata=None)
        writer = ProtoWriter(Path("../"), TimeSeriesManager())
        ProtoWriter._add_agents(writer, pb_input=pb_input, agents=[agent], schema=self._schema)
        self._assert_agent_data_match_stored_data(agent, pb_input.agents[0])

    @staticmethod
    def _create_agent(
        agent_id: int = 42, type_name: str = "MyAgent", attributes: dict = None, metadata: dict = None
    ) -> Agent:
        """Creates an agent from given data"""
        return Agent.from_dict(
            TestAgent.new_agent(agent_id=agent_id, type_name=type_name, attributes=attributes, metadata=metadata)
        )

    @staticmethod
    def _extract_agents_from_protobuf(message: InputData) -> list[dict]:
        """Extract all stored agents from given InputData protobuf message"""
        return MessageToDict(message)["agent"]

    @staticmethod
    def _assert_agent_data_match_stored_data(
        agent: Agent, agent_protobuf: InputData.AgentDao, timeseries_manager: TimeSeriesManager = None
    ) -> None:
        """Asserts that given representations of agent in scenario and protobuf match"""
        assert agent_protobuf.id == agent.id
        assert agent_protobuf.class_name == agent.type_name
        assert agent_protobuf.metadata == agent.get_metadata_string()
        for index, name in enumerate(agent.attributes.keys()):
            TestProtoWriter._assert_attribute_match(
                name, agent.attributes[name], agent_protobuf.fields[index], timeseries_manager
            )

    @staticmethod
    def _assert_attribute_match(
        name: str, attribute: Attribute, field: NestedField, timeseries_manager: TimeSeriesManager = None
    ) -> None:
        """Asserts that given representations of attribute in scenario and protobuf match"""
        assert field.field_name == name
        if attribute.has_nested:
            for index, name in enumerate(attribute.nested.keys()):
                TestProtoWriter._assert_attribute_match(
                    name, attribute.nested[name], field.fields[index], timeseries_manager
                )
        if isinstance(attribute.value, list):
            for index, item in enumerate(attribute.value):
                TestProtoWriter._assert_value_match(item, index, field, timeseries_manager)
        else:
            TestProtoWriter._assert_value_match(attribute.value, 0, field, timeseries_manager)

    @staticmethod
    def _assert_value_match(
        value: Union[str, float, int], index: int, field: NestedField, timeseries_manager: TimeSeriesManager
    ):
        """Asserts that given value is found as the index's value of the given field's data arrays"""
        if field.HasField("series_id"):
            assert field.series_id == timeseries_manager.get_series_id_by_identifier(value)
        elif len(field.int_values) > 0:
            assert field.int_values[index] == value
        elif len(field.long_values) > 0:
            assert field.long_values[index] == value
        elif len(field.string_values) > 0:
            assert field.string_values[index] == value
        elif len(field.double_values) > 0:
            assert field.double_values[index] == value
        elif len(field.long_values) > 0:
            assert field.long_values[index] == value

    def test__add_agents__one_agent_with_metadata__added(self):
        pb_input = self._initialize_pb_input()
        metadata = {"MyMetaData": "VeryDetailed"}
        agent = self._create_agent(agent_id=42, type_name="MyAgent", attributes=None, metadata=metadata)
        writer = ProtoWriter(Path("../"), TimeSeriesManager())
        ProtoWriter._add_agents(writer, pb_input=pb_input, agents=[agent], schema=self._schema)
        self._assert_agent_data_match_stored_data(agent, pb_input.agents[0])

    @pytest.mark.parametrize(
        "attributes",
        [
            {"Int": 2},
            {"Double": 5.2},
            {"DoubleList": [2.0, 3.0]},
            {"Enum": "MyEnum"},
            {"StringSet": "MySetValue"},
            {"Long": 555},
            {"Block": {"InnerInt": 5}},
            {"BlockList": [{"InnerDouble": 4.2}, {"InnerDouble": 8.4}]},
        ],
    )
    def test__add_agent__one_agent_with_attribute__added(self, attributes):
        pb_input = self._initialize_pb_input()
        agent = self._create_agent(agent_id=42, type_name="MyAgent", attributes=attributes, metadata=None)
        writer = ProtoWriter(Path("../"), TimeSeriesManager())
        ProtoWriter._add_agents(writer, pb_input=pb_input, agents=[agent], schema=self._schema)
        self._assert_agent_data_match_stored_data(agent, pb_input.agents[0])

    def test__add_agent__one_agent_with_series__added(self):
        pb_input = self._initialize_pb_input()
        attributes = {"Series": 5}
        agent = self._create_agent(agent_id=42, type_name="MyAgent", attributes=attributes, metadata=None)
        timeseries_manager = TimeSeriesManager()
        writer = ProtoWriter(Path("../"), timeseries_manager)
        timeseries_manager.register_and_validate(5)
        ProtoWriter._add_agents(writer, pb_input=pb_input, agents=[agent], schema=self._schema)
        self._assert_agent_data_match_stored_data(agent, pb_input.agents[0], timeseries_manager)

    def test__add_agents__multiple_agents__all_added(self):
        pb_input = self._initialize_pb_input()
        meta_data = {"MyMetaData": "VeryDetailed"}
        agent_1 = self._create_agent(agent_id=42, type_name="MyAgent", attributes={"Int": 2}, metadata=meta_data)
        agent_2 = self._create_agent(
            agent_id=43, type_name="MyAgent", attributes={"DoubleList": [2.0, 3.0]}, metadata=None
        )
        writer = ProtoWriter(Path("../"), TimeSeriesManager())
        ProtoWriter._add_agents(writer, pb_input=pb_input, agents=[agent_1, agent_2], schema=self._schema)
        self._assert_agent_data_match_stored_data(agent_1, pb_input.agents[0])
        self._assert_agent_data_match_stored_data(agent_2, pb_input.agents[1])

    def test__add_contracts__one_contract_no_attributes__added(self):
        pb_input = self._initialize_pb_input()
        contract = TestContract.contract_from_dict(1, 2, "MyProduct", 0, 10, 1000)
        writer = ProtoWriter(Path("../"), TimeSeriesManager())
        writer._add_contracts(pb_input, contracts=[contract])
        self._assert_contract_data_match_stored_data(contract, pb_input.contracts[0])

    @staticmethod
    def _assert_contract_data_match_stored_data(contract: Contract, pb_contract: ProtoContract) -> None:
        """Asserts that given representations of contract match"""
        assert pb_contract.sender_id == contract.sender_id
        assert pb_contract.receiver_id == contract.receiver_id
        assert pb_contract.product_name == contract.product_name
        assert pb_contract.first_delivery_time == contract.first_delivery_time
        assert pb_contract.delivery_interval_in_steps == contract.delivery_interval
        if contract.expiration_time:
            assert pb_contract.expiration_time == contract.expiration_time
        if contract.metadata:
            assert pb_contract.metadata == contract.get_metadata_string()
        for index, name in enumerate(contract.attributes.keys()):
            TestProtoWriter._assert_attribute_match(name, contract.attributes[name], pb_contract.fields[index])

    def test__add_contracts__one_contract_with_attributes__added(self):
        pb_input = self._initialize_pb_input()
        contract = TestContract.contract_from_dict(
            sender=1,
            receiver=2,
            product="MyProduct",
            first=0,
            steps=10,
            attributes={"Int": 5, "Double": 4.2, "Enum": "MyEnum"},
        )
        writer = ProtoWriter(Path("../"), TimeSeriesManager())
        writer._add_contracts(pb_input, contracts=[contract])
        self._assert_contract_data_match_stored_data(contract, pb_input.contracts[0])

    def test__add_contracts__one_contract_with_metadata__added(self):
        pb_input = self._initialize_pb_input()
        contract = TestContract.contract_from_dict(
            sender=1,
            receiver=2,
            product="MyProduct",
            first=0,
            steps=10,
            meta={"Some": {"Complex": {"Metadata": "Structure"}}},
        )
        writer = ProtoWriter(Path("../"), TimeSeriesManager())
        writer._add_contracts(pb_input, contracts=[contract])
        self._assert_contract_data_match_stored_data(contract, pb_input.contracts[0])

    def test__add_contracts__multiple_contracts_added__all_added(self):
        pb_input = self._initialize_pb_input()
        meta_data = {"MyMetaData": "VeryDetailed"}
        contract_1 = TestContract.contract_from_dict(
            sender=1,
            receiver=12,
            product="MyProduct",
            first=0,
            steps=10,
            expire=1000,
            meta=None,
            attributes=None,
        )
        contract_2 = TestContract.contract_from_dict(
            sender=12,
            receiver=1,
            product="MyProduct",
            first=0,
            steps=10,
            expire=1000,
            meta=meta_data,
            attributes=None,
        )
        writer = ProtoWriter(Path("../"), TimeSeriesManager())
        writer._add_contracts(pb_input, contracts=[contract_1, contract_2])
        self._assert_contract_data_match_stored_data(contract_1, pb_input.contracts[0])
        self._assert_contract_data_match_stored_data(contract_2, pb_input.contracts[1])

    def test__set_schema__schema_set__all_set(self):
        pb_input = self._initialize_pb_input()
        ProtoWriter._set_schema(pb_input=pb_input, schema=self._schema)
        assert pb_input.schema == self._schema.to_string()

    def test__set_time_series__series_manager_is_empty__no_series(self):
        pb_input = self._initialize_pb_input()
        writer = ProtoWriter(Path("../"), TimeSeriesManager())
        writer._set_time_series(pb_input)
        assert len(pb_input.time_series) == 0

    def test__set_time_series__all_series_set__all_set(self):
        pb_input = self._initialize_pb_input()
        ts_manager = TimeSeriesManager()
        ts_manager.register_and_validate(0)
        ts_manager.register_and_validate(2)
        writer = ProtoWriter(Path("../"), ts_manager)
        writer._set_time_series(pb_input)
        assert len(pb_input.time_series) == 2

    def test__write_protobuf_to_disk__events_logged(self, fame_log, stub_writing, un_stub):
        writer = ProtoWriter(Path(), mock(TimeSeriesManager))
        mock_data = mock(DataStorage)
        when(mock_data).SerializeToString().thenReturn("")
        fame_log.set_level(logging.INFO)

        writer._write_protobuf_to_disk(mock_data)
        fame_log.assert_logged(ProtoWriter._INFO_WRITING)
        fame_log.assert_logged(ProtoWriter._INFO_WRITING_COMPLETED)
        verify(stub_writing, times=1).write(any(str))

    def test__write_protobuf_to_disk__invalid_file__raises(self, fame_log, stub_writing, un_stub):
        writer = ProtoWriter(Path(), mock(TimeSeriesManager))
        mock_data = mock(DataStorage)
        when(mock_data).SerializeToString().thenReturn("")
        when(stub_writing).write(any(str)).thenRaise(OSError())
        with pytest.raises(ProtoWriterError) as e_info:
            writer._write_protobuf_to_disk(mock_data)
        fame_log.assert_logged_exception(ProtoWriter._NO_FILE_SPECIFIED, e_info)

    def test__write_validated_scenario__all_set(self, stub_writing, un_stub):
        ts_manager = mock(TimeSeriesManager)
        when(ts_manager).get_all_series().thenReturn([])
        writer = ProtoWriter(Path(), ts_manager)
        patch(importlib.metadata.version, lambda _: "MyVersion")

        scenario = Scenario.from_dict(
            {
                Scenario.KEY_SCHEMA: self._schema.to_dict(),
                Scenario.KEY_GENERAL: self._initialize_general_properties().to_dict(),
                Scenario.KEY_AGENTS: [self._create_agent(agent_id=42, type_name="MyAgent").to_dict()],
                Scenario.KEY_CONTRACTS: [
                    TestContract.contract_from_dict(
                        sender=1,
                        receiver=1,
                        product="MyProduct",
                        first=0,
                        steps=10,
                    ).to_dict()
                ],
            }
        )

        writer.write_validated_scenario(scenario)
        verify(stub_writing, times=3).write(any())

    def test__set_string_sets__empty__does_nothing(self):
        pb_input = self._initialize_pb_input()
        ProtoWriter._set_string_sets(pb_input, {})
        assert len(pb_input.string_sets) == 0

    def test__set_string_sets__one_no_meta__added(self):
        pb_input = self._initialize_pb_input()
        string_sets = {"MySet": self._create_string_set({}, [("A", {}), ("B", {})])}
        ProtoWriter._set_string_sets(pb_input, string_sets)
        self._assert_string_sets_defined(pb_input, string_sets)

    @staticmethod
    def _create_string_set(meta: dict, values_plus_metadata: list[tuple[str, dict]]) -> StringSet:
        """Returns a new StringSet with given metadata and values including their value-specific metadata"""
        values = {}
        for name, value_meta in values_plus_metadata:
            values[name] = {StringSet.KEY_METADATA: value_meta} if value_meta else {}
        return StringSet.from_dict({StringSet.KEY_METADATA: meta, StringSet.KEY_VALUES: values})

    @staticmethod
    def _assert_string_sets_defined(pb_input: InputData, string_sets: dict[str, StringSet]) -> None:
        """Asserts that all data from given `string_sets` are stored in given `pb_input`"""
        pb_sets = pb_input.string_sets
        assert len(pb_sets) == len(string_sets)
        for index, name in enumerate(string_sets):
            pb_set = pb_sets[index]
            string_set = string_sets[name]
            assert name == pb_set.name
            assert string_set.get_metadata_string() == pb_set.metadata
            assert len(string_set.values) == len(pb_set.values)
            for value_index, value_name in enumerate(string_set.values):
                pb_value = pb_set.values[value_index]
                value_spec = string_set.values[value_name]
                assert value_name == pb_value.name
                assert value_spec.get_metadata_string() == pb_value.metadata

    def test__set_string_sets__one_meta__added(self):
        pb_input = self._initialize_pb_input()
        string_sets = {"MySet": self._create_string_set({"My": "TopLevel", "Meta": "Data"}, [("A", {}), ("B", {})])}
        ProtoWriter._set_string_sets(pb_input, string_sets)
        self._assert_string_sets_defined(pb_input, string_sets)

    def test__set_string_sets__one_value_meta__added(self):
        pb_input = self._initialize_pb_input()
        metadata = {"My": "Value", "Meta": "Data"}
        string_sets = {"MySet": self._create_string_set({}, [("A", metadata), ("B", metadata)])}
        ProtoWriter._set_string_sets(pb_input, string_sets)
        self._assert_string_sets_defined(pb_input, string_sets)

    def test__set_string_sets__two_no_meta__added(self):
        pb_input = self._initialize_pb_input()
        string_sets = {
            "SetA": self._create_string_set({}, [("A", {}), ("B", {})]),
            "SetB": self._create_string_set({}, [("X", {}), ("Y", {})]),
        }
        ProtoWriter._set_string_sets(pb_input, string_sets)
        self._assert_string_sets_defined(pb_input, string_sets)

    def test__set_string_sets__two_meta__added(self):
        pb_input = self._initialize_pb_input()
        string_sets = {
            "SetA": self._create_string_set({"My": "TopLevel", "Meta": "Data"}, [("A", {}), ("B", {})]),
            "SetB": self._create_string_set({"Or": "Some", "Other": "Things"}, [("X", {}), ("Y", {})]),
        }
        ProtoWriter._set_string_sets(pb_input, string_sets)
        self._assert_string_sets_defined(pb_input, string_sets)

    def test__set_string_sets__two_value_meta__added(self):
        pb_input = self._initialize_pb_input()
        string_sets = {
            "SetA": self._create_string_set({}, [("A", {1: 2, 3: 4}), ("B", {5: 6, 7: 8})]),
            "SetB": self._create_string_set({}, [("X", {}), ("Y", {2: 3, 5: 7})]),
        }
        ProtoWriter._set_string_sets(pb_input, string_sets)
        self._assert_string_sets_defined(pb_input, string_sets)

    def test__set_java_package_names__agents__added(self):
        pb_model = self._initialize_pb_model()
        expected = ["myAgentPackage1", "myAgentPackage2"]
        packages = self._mock_java_packages(expected, [], [])

        ProtoWriter._set_java_package_names(pb_model, packages)
        assert list(pb_model.package_definition.agents) == expected

    @staticmethod
    def _initialize_pb_model() -> ModelData:
        """Returns new empty `ModelData` of a new DataStorage"""
        pb_data_storage = DataStorage()
        return pb_data_storage.model

    @staticmethod
    def _mock_java_packages(agents: list[str], data_items: list[str], portables: list[str]) -> JavaPackages:
        packages = mock(JavaPackages)
        packages.agents = agents
        packages.data_items = data_items
        packages.portables = portables
        return packages

    def test__set_java_package_names__data_items__added(self):
        pb_model = self._initialize_pb_model()
        expected = ["myDataItemPackage1", "myDataItemPackage2"]
        packages = self._mock_java_packages([], expected, [])

        ProtoWriter._set_java_package_names(pb_model, packages)
        assert list(pb_model.package_definition.data_items) == expected

    def test__set_java_package_names__portables__added(self):
        pb_model = self._initialize_pb_model()
        expected = ["myPortablePackage1", "myPortablePackage2"]
        packages = self._mock_java_packages([], [], expected)

        ProtoWriter._set_java_package_names(pb_model, packages)
        assert list(pb_model.package_definition.portables) == expected

    @staticmethod
    def _initialize_pb_versions() -> ExecutionData.VersionData:
        """Returns new empty `Versions` of a new DataStorage.ExecutionData"""
        pb_data_storage = DataStorage()
        return pb_data_storage.execution.version_data

    def test__set_execution_versions__(self, un_stub):
        pb_versions = self._initialize_pb_versions()
        patch(importlib.metadata.version, lambda _: "MyVersion")
        ProtoWriter._set_execution_versions(pb_versions)
        assert len(pb_versions.python) > 0
        assert pb_versions.fame_io == "MyVersion"
        assert pb_versions.fame_protobuf == "MyVersion"
