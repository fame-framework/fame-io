# SPDX-FileCopyrightText: 2023 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
import copy
from typing import Union, Any

import pytest
from mockito import when

from fameio.input import SchemaError
from fameio.input.schema import AttributeSpecs, AttributeType
from fameio.time import FameTime
from tests.utils import assert_exception_contains, new_attribute


def make_attribute(
    name: str,
    data_type: str = None,
    is_mandatory: bool = None,
    is_list: bool = None,
    nested_list: list = None,
    values: Any = None,
    default: Union[str, list[str]] = None,
    help_string: str = None,
    metadata: dict = None,
) -> AttributeSpecs:
    """Returns new built attribute with given parameters"""
    definition = new_attribute(
        name, data_type, is_mandatory, is_list, nested_list, values, default, help_string, metadata
    )[name]
    return AttributeSpecs(name, definition)


class Test:
    @pytest.mark.parametrize("name", [None, "", " ", "some.", "is.missing."])
    def test__init__name_empty__raises(self, name):
        with pytest.raises(SchemaError) as e_info:
            AttributeSpecs(name, None)  # noqa
        assert_exception_contains(AttributeSpecs._NAME_DISALLOWED, e_info)

    @pytest.mark.parametrize("name", ["value", "values", "metadata", "some.value", "deep.path.to.values"])
    def test__init__name_disallowed__raises(self, name):
        with pytest.raises(SchemaError) as e_info:
            AttributeSpecs(name, None)  # noqa
        assert_exception_contains(AttributeSpecs._NAME_DISALLOWED, e_info)

    def test__init__none_definition__raises(self):
        with pytest.raises(SchemaError) as e_info:
            AttributeSpecs("AnyName", None)  # noqa
        assert_exception_contains(AttributeSpecs._EMPTY_DEFINITION, e_info)

    def test__init__empty_definition__raises(self):
        with pytest.raises(SchemaError) as e_info:
            AttributeSpecs("AnyName", {})  # noqa
        assert_exception_contains(AttributeSpecs._EMPTY_DEFINITION, e_info)

    def test__init__type_missing__raises(self):
        with pytest.raises(SchemaError) as e_info:
            make_attribute("OIL", None, False, False)
        assert_exception_contains(AttributeSpecs._MISSING_TYPE, e_info)

    def test__init__type_unknown__raises(self):
        with pytest.raises(SchemaError) as e_info:
            make_attribute("OIL", "xxx", False, False)
        assert_exception_contains(AttributeSpecs._INVALID_TYPE, e_info)

    @pytest.mark.parametrize("data_type", ["double", "DOUBLE", "dOUbLe"])
    def test__init__type_case__does_not_matter(self, data_type):
        assert make_attribute("OIL", data_type, False, False)

    def test__init__mandatory_missing__assume_mandatory(self):
        attribute_type = make_attribute("OIL", "double", None, False)
        assert attribute_type.is_mandatory

    @pytest.mark.parametrize("value", [True, False])
    def test__init__mandatory_given__set(self, value):
        attribute_type = make_attribute("OIL", "double", value, False)
        assert attribute_type.is_mandatory == value

    def test__init__list_missing__assume_no_list(self):
        attribute_type = make_attribute("OIL", "double", False, None)
        assert not attribute_type.is_list

    @pytest.mark.parametrize("value", [True, False])
    def test__init__list_given__set(self, value):
        attribute_type = make_attribute("OIL", "double", False, value)
        assert attribute_type.is_list == value

    def test__init__attribute_list_but_time_series__raises(self):
        with pytest.raises(SchemaError) as e_info:
            make_attribute("OIL", "time_series", False, True)
        assert_exception_contains(AttributeSpecs._SERIES_LIST_DISALLOWED, e_info)

    def test__init__values_none__values_empty(self):
        attribute_type = make_attribute("OIL", "double", False, False, None, None)
        assert attribute_type.values == []

    def test__init__values_empty__values_empty(self):
        attribute_type = make_attribute("OIL", "double", False, False, None, [])
        assert attribute_type.values == []

    def test__init__values_list__values_set(self):
        values = [42, 1.0, 2]
        attribute_type = make_attribute("OIL", "double", False, False, None, values.copy())
        assert attribute_type.values == values

    def test__init__values_dict__values_set_to_keys(self):
        values = {"42": {"Inner": "stuff"}, 1.0: 0, 2: "F"}
        attribute_type = make_attribute("OIL", "double", False, False, None, values)
        assert attribute_type.values == [k for k in values.keys()]

    def test__init__values_tuple__raises(self):
        with pytest.raises(SchemaError) as e_info:
            make_attribute("OIL", "double", False, False, None, ("1", "2"))
        assert_exception_contains(AttributeSpecs._VALUES_ILL_FORMAT, e_info)

    def test__init__has_value_restrictions__apply__returns_true(self):
        attribute_type = make_attribute("OIL", "integer", False, False, None, [1, 2, 3])
        assert attribute_type.has_value_restrictions

    def test__init__values_primitive__raises(self):
        with pytest.raises(SchemaError) as e_info:
            make_attribute("OIL", "double", False, False, None, "NotAllowed")
        assert_exception_contains(AttributeSpecs._VALUES_ILL_FORMAT, e_info)

    def test__init__values_wrong_type__raises(self):
        with pytest.raises(SchemaError) as e_info:
            make_attribute("OIL", "double", False, False, None, ["NotAnInteger", 5])
        assert_exception_contains(AttributeSpecs._INCOMPATIBLE, e_info)

    def test__has_nested__attributes_missing__returns_false(self):
        attribute_type = make_attribute("OIL", "double", True, False)
        assert not attribute_type.has_nested_attributes

    def test__has_nested__attributes_present__returns_true(self):
        attribute_type = make_attribute("OIL", "block", True, False, [new_attribute("Inner", "double", True, False)])
        assert attribute_type.has_nested_attributes

    def test__has_default__default_given__returns_true(self):
        attribute_type = make_attribute("OIL", "double", True, False, None, None, "5.2")
        assert attribute_type.has_default_value

    def test__has_default__no_default__returns_false(self):
        attribute_type = make_attribute("OIL", "double", True, False, None, None, None)
        assert not attribute_type.has_default_value

    @pytest.mark.parametrize(
        "data_type,default,expected",
        [
            ("integer", "5", 5),
            ("double", "4.2", 4.2),
            ("double", "42", 42),
            ("string_set", "X", "X"),
            ("enum", "A", "A"),
            ("string", "B", "B"),
            ("time_series", "4.2", 4.2),
        ],
    )
    def test__init__default_value__converted_to_correct_type(self, data_type, default, expected):
        attribute_type = make_attribute("OIL", data_type, True, False, None, None, default)
        assert attribute_type.default_value == expected

    @pytest.mark.parametrize(
        "data_type,default",
        [
            ("integer", "5.2"),
            ("integer", "7h15 15 n07 an 1n75g5r"),
            ("double", "7h15 15"),
            ("time_series", "NotATimeStamp"),
            ("block", "AnyThing"),
        ],
    )
    def test__init__default_value_wrong_data_type__raises(self, data_type, default):
        with pytest.raises(SchemaError) as e_info:
            make_attribute("OIL", data_type, True, False, None, None, default)
        assert_exception_contains(AttributeSpecs._INCOMPATIBLE, e_info)

    @pytest.mark.parametrize(
        "data_type,values,default",
        [
            ("string", ["A"], "B"),
            ("double", [4.2], 2.4),
            ("integer", [42], 24),
            ("string_set", ["X"], "Z"),
            ("enum", ["E"], "N"),
        ],
    )
    def test__init__default_not_in_values__raises(self, data_type, values, default):
        with pytest.raises(SchemaError) as e_info:
            make_attribute("OIL", data_type, True, False, None, values, default)
        assert_exception_contains(AttributeSpecs._DEFAULT_DISALLOWED, e_info)

    @pytest.mark.parametrize(
        "data_type,values,default",
        [
            ("string", ["A"], "A"),
            ("double", [4.2], 4.2),
            ("integer", [42], 42),
            ("string_set", ["X"], "X"),
            ("enum", ["E"], "E"),
        ],
    )
    def test__init__default_in_values__passes(self, data_type, values, default):
        make_attribute("OIL", data_type, True, False, None, values, default)

    def test__init__time_stamp_default__converted_to_int(self):
        with when(FameTime).convert_string_if_is_datetime(any).thenReturn(1):
            attribute_type = make_attribute("OIL", "time_stamp", True, False, None, None, "isIgnored")
        assert attribute_type.default_value == 1

    def test__init__is_list_but_default_is_no_list__raises(self):
        with pytest.raises(SchemaError) as e_info:
            make_attribute("OIL", "integer", True, True, None, None, "5")
        assert_exception_contains(AttributeSpecs._DEFAULT_NOT_LIST, e_info)

    def test__init__is_list_default_is_not_in_allowed_values__raises(self):
        with pytest.raises(SchemaError) as e_info:
            make_attribute("OIL", "integer", True, True, None, [1, 2, 3], ["5"])
        assert_exception_contains(AttributeSpecs._DEFAULT_DISALLOWED, e_info)

    def test__init__is_list_default_has_rong_type__raises(self):
        with pytest.raises(SchemaError) as e_info:
            make_attribute("OIL", "integer", True, True, None, None, ["5.2"])
        assert_exception_contains(AttributeSpecs._INCOMPATIBLE, e_info)

    def test__init__is_list_with_compatible_list_default__default_set(self):
        attribute_type = make_attribute("OIL", "integer", True, True, None, None, ["1", "1", "2", "3"])
        assert attribute_type.default_value == [1, 1, 2, 3]

    def test__init__with_help_definition__is_set(self):
        attribute_type = make_attribute("OIL", "integer", True, False, None, None, None, "This is help")
        assert attribute_type.has_help_text
        assert attribute_type.help_text == "This is help"

    def test__init__no_help__help_empty(self):
        attribute_type = make_attribute("OIL", "integer", True, False, None, None, None, None)
        assert not attribute_type.has_help_text
        assert attribute_type.help_text == ""

    def test__init__metadata__set(self):
        metadata = {"A": 1, "B": "bla"}
        attribute_type = make_attribute(name="OIL", data_type="integer", metadata=metadata)
        assert attribute_type.metadata == metadata

    def test__init__no_metadata__not_set(self):
        attribute_type = make_attribute(name="OIL", data_type="integer", metadata=None)
        assert attribute_type.metadata == {}

    def test__init__to_dict__contains_type(self):
        attribute_type = make_attribute(name="OIL", data_type="integer")
        assert attribute_type.to_dict().get(AttributeSpecs.KEY_TYPE) == AttributeType.INTEGER.name

    def test__init__to_dict__contains_mandatory(self):
        attribute_type = make_attribute(name="OIL", data_type="integer", is_mandatory=False)
        assert not attribute_type.to_dict().get(AttributeSpecs.KEY_MANDATORY)

    def test__init__to_dict__contains_list(self):
        attribute_type = make_attribute(name="OIL", data_type="integer", is_list=True)
        assert attribute_type.to_dict().get(AttributeSpecs.KEY_LIST)

    def test__init__to_dict__has_help__contains_help(self):
        help_string = "My help string"
        attribute_type = make_attribute(name="OIL", data_type="integer", help_string=help_string)
        assert attribute_type.to_dict().get(AttributeSpecs.KEY_HELP) == help_string

    @pytest.mark.parametrize("help_string", [None, "", "  "])
    def test__init__to_dict__no_help__no_help_key(self, help_string):
        attribute_type = make_attribute(name="OIL", data_type="integer", help_string=help_string)
        assert AttributeSpecs.KEY_HELP not in attribute_type.to_dict()

    def test__init__to_dict__has_default__contains_default(self):
        attribute_type = make_attribute(name="OIL", data_type="integer", default="1")
        assert attribute_type.to_dict().get(AttributeSpecs.KEY_DEFAULT) == 1

    def test__init__to_dict__no_default__no_default_key(self):
        attribute_type = make_attribute(name="OIL", data_type="integer", default=None)
        assert AttributeSpecs.KEY_DEFAULT not in attribute_type.to_dict()

    def test__init__to_dict__has_restrictions__contains_restrictions(self):
        attribute_type = make_attribute(name="OIL", data_type="integer", values=[1, 2, 3])
        assert attribute_type.to_dict().get(AttributeSpecs.KEY_VALUES) == {1: {}, 2: {}, 3: {}}

    def test__init__to_dict__no_restrictions__no_restriction_key(self):
        attribute_type = make_attribute(name="OIL", data_type="integer", values=None)
        assert AttributeSpecs.KEY_VALUES not in attribute_type.to_dict()

    def test__init__to_dict__restrictions_with_metadata__metadata_contained(self):
        values = {"A": {}, "B": {AttributeSpecs.KEY_METADATA: {"Some": 1, "Metadata": "Here"}}}
        attribute_type = make_attribute(name="OIL", data_type="string", values=copy.deepcopy(values))
        assert attribute_type.to_dict().get(AttributeSpecs.KEY_VALUES) == values

    def test__init__to_dict__no_nested__no_nested_key(self):
        attribute_type = make_attribute(name="OIL", data_type="integer", nested_list=[])
        assert AttributeSpecs.KEY_NESTED not in attribute_type.to_dict()

    def test__init__to_dict__nested__contains_nested_key(self):
        attribute_type = make_attribute(
            name="OIL",
            data_type="integer",
            nested_list=[
                new_attribute(name="InnerA", data_type="double"),
                new_attribute(name="InnerB", data_type="double"),
            ],
        )
        assert AttributeSpecs.KEY_NESTED in attribute_type.to_dict().keys()

    def test__init__to_dict__nested_items__contains_nested_items(self):
        attribute_type = make_attribute(
            name="OIL",
            data_type="integer",
            nested_list=[
                new_attribute(name="InnerA", data_type="double"),
                new_attribute(name="InnerB", data_type="double"),
            ],
        )
        nested = attribute_type.to_dict().get(AttributeSpecs.KEY_NESTED)
        assert isinstance(nested.get("InnerA"), dict)
        assert isinstance(nested.get("InnerB"), dict)
