# SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
import logging

import pytest

from fameio.input import SchemaError
from fameio.input.schema import JavaPackages
from tests.log_utils import fame_log


class TestJavaPackages:
    _definitions = {
        JavaPackages.KEY_AGENT: ["AgentPackage1", "AgentPackage2"],
        JavaPackages.KEY_DATA_ITEM: ["DataItemPackage1", "DataItemPackage2"],
        JavaPackages.KEY_PORTABLE: ["PortablePackage1", "PortablePackage2"],
    }

    def test__init__creates_empty_packages(self):
        packages = JavaPackages()
        assert packages.agents == []
        assert packages.data_items == []
        assert packages.portables == []

    def test__from_dict__missing_key_agent__logged_and_raised(self, fame_log):
        definitions = {JavaPackages.KEY_DATA_ITEM: ["A"], JavaPackages.KEY_PORTABLE: ["B"]}
        with pytest.raises(SchemaError) as e_info:
            JavaPackages.from_dict(definitions)
        fame_log.assert_logged_exception(JavaPackages._ERR_MISSING_AGENTS, e_info)

    def test__from_dict__empty_list_agent__logged_and_raised(self, fame_log):
        definitions = {
            JavaPackages.KEY_AGENT: [],
            JavaPackages.KEY_DATA_ITEM: ["A"],
            JavaPackages.KEY_PORTABLE: ["B"],
        }
        with pytest.raises(SchemaError) as e_info:
            JavaPackages.from_dict(definitions)
        fame_log.assert_logged_exception(JavaPackages._ERR_MISSING_AGENTS, e_info)

    def test__from_dict__missing_key_data_item__logged_info(self, fame_log):
        fame_log.set_level(logging.INFO)
        definitions = {JavaPackages.KEY_AGENT: ["A"], JavaPackages.KEY_PORTABLE: ["B"]}
        JavaPackages.from_dict(definitions)
        fame_log.assert_logged(JavaPackages._INFO_MISSING_DATA_ITEMS)

    def test__from_dict__empty_list_data_item__logged_and_raised(self, fame_log):
        fame_log.set_level(logging.INFO)
        definitions = {
            JavaPackages.KEY_AGENT: ["A"],
            JavaPackages.KEY_DATA_ITEM: [],
            JavaPackages.KEY_PORTABLE: ["B"],
        }
        JavaPackages.from_dict(definitions)
        fame_log.assert_logged(JavaPackages._INFO_MISSING_DATA_ITEMS)

    def test__from_dict__missing_key_portable__logged_and_raised(self, fame_log):
        definitions = {JavaPackages.KEY_AGENT: ["A"], JavaPackages.KEY_DATA_ITEM: ["B"]}
        with pytest.raises(SchemaError) as e_info:
            JavaPackages.from_dict(definitions)
        fame_log.assert_logged_exception(JavaPackages._ERR_MISSING_PORTABLES, e_info)

    def test__from_dict__empty_list_portable__logged_and_raised(self, fame_log):
        definitions = {
            JavaPackages.KEY_AGENT: ["A"],
            JavaPackages.KEY_DATA_ITEM: ["B"],
            JavaPackages.KEY_PORTABLE: [],
        }
        with pytest.raises(SchemaError) as e_info:
            JavaPackages.from_dict(definitions)
        fame_log.assert_logged_exception(JavaPackages._ERR_MISSING_PORTABLES, e_info)

    def test__from_dict__agents__assigned(self):
        packages = JavaPackages.from_dict(self._definitions)
        assert packages.agents == self._definitions[JavaPackages.KEY_AGENT]

    def test__from_dict__data_items__assigned(self):
        packages = JavaPackages.from_dict(self._definitions)
        assert packages.data_items == self._definitions[JavaPackages.KEY_DATA_ITEM]

    def test__from_dict__portables__assigned(self):
        packages = JavaPackages.from_dict(self._definitions)
        assert packages.portables == self._definitions[JavaPackages.KEY_PORTABLE]

    def test__from_dict_to_dict__return_input(self):
        packages = JavaPackages.from_dict(self._definitions)
        assert packages.to_dict() == self._definitions
