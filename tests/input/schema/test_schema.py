# SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0

import pytest
from mockito import when, mock, unstub

from fameio.input import SchemaError
from fameio.input.schema import Schema, JavaPackages
from tests.utils import assert_exception_contains

_package_definition_dummy = {"AnyKey": "AnyValue"}


@pytest.fixture
def mock_java_packages() -> JavaPackages:
    """Mocks JavaPackages"""
    mock_packages = mock(JavaPackages)
    when(JavaPackages).from_dict(_package_definition_dummy).thenReturn(mock_packages)
    yield mock_packages
    unstub()


class Test:
    _package_definition = {Schema.KEY_PACKAGES: _package_definition_dummy}

    def test__from_dict__empty__raises(self):
        with pytest.raises(SchemaError) as e_info:
            Schema.from_dict({})
        assert_exception_contains(Schema._ERR_AGENT_TYPES_MISSING, e_info)

    def test__from_dict__empty_agents__raises(self):
        with pytest.raises(SchemaError) as e_info:
            Schema.from_dict({Schema.KEY_AGENT_TYPE: {}})
        assert_exception_contains(Schema._ERR_AGENT_TYPES_EMPTY, e_info)

    def test__from_dict__to_dict__returns_identity(self, mock_java_packages):
        definitions = {Schema.KEY_AGENT_TYPE: {"Agent": {}, "OtherAgent": {}}, **Test._package_definition}
        schema = Schema.from_dict(definitions)
        assert schema.to_dict() == definitions

    def test__from_string__of_to_string__returns_original_dict(self, mock_java_packages):
        definitions = {Schema.KEY_AGENT_TYPE: {"Agent": {}, "OtherAgent": {}}, **Test._package_definition}
        schema = Schema.from_dict(definitions)
        string_representation = schema.to_string()
        second_schema = Schema.from_string(string_representation)
        assert second_schema.to_dict() == definitions

    def test__from_dict__mixed_caps_key_word__accepted_and_turned_lower_case(self, mock_java_packages):
        types = {"Agent": {}, "OtherAgent": {}}
        definitions = {"AgEntTYPeS": types, **Test._package_definition}
        schema = Schema.from_dict(definitions)
        assert schema.to_dict()[Schema.KEY_AGENT_TYPE] == types

    def test__from_dict__agent_types__contained_in_keys(self, mock_java_packages):
        types = {"Agent": {}, "OtherAgent": {}, "ThirdAgent": {}}
        schema = Schema.from_dict({Schema.KEY_AGENT_TYPE: types, **Test._package_definition})
        assert set(schema.agent_types.keys()) == set(types.keys())

    def test__from_dict__no_packages__raises(self):
        with pytest.raises(SchemaError) as e_info:
            Schema.from_dict({Schema.KEY_AGENT_TYPE: {"Agent": {}}})
        assert_exception_contains(Schema._ERR_MISSING_PACKAGES, e_info)

    def test__from_dict__with_packages__creates_java_packages(self, mock_java_packages):
        schema = Schema.from_dict({Schema.KEY_AGENT_TYPE: {"Agent": {}}, **Test._package_definition})
        assert schema.packages == mock_java_packages
