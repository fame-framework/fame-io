# SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
from pathlib import Path

import pytest
from mockito import mock, when

from fameio.input.loader import (
    PathResolver,
    load_yaml,
    validate_yaml_file_suffix,
    FameYamlLoader,
)
from tests.mock_utils import un_stub, change_dir


class Test:
    def test__init__stream_name_none__sets_root_path_to_current_directory(self, monkeypatch, un_stub):
        stream = mock()
        stream.name = None
        when(stream).read(4096).thenReturn("")
        reference_path = Path("This/is/My/Path")
        monkeypatch.setattr("os.path.curdir", reference_path)
        loader = FameYamlLoader(stream)
        assert loader._root_path == reference_path

    def test__init__stream_name_exists__sets_root_path_to_stream_parent_directory(self, un_stub, monkeypatch):
        stream = mock()
        parent = "This/is/My/Path"
        stream.name = parent + "/" + "filename.xyz"
        when(stream).read(4096).thenReturn("")
        reference_path = Path("This/is/My/Path")
        monkeypatch.setattr("os.path.curdir", reference_path)
        loader = FameYamlLoader(stream)
        assert loader._root_path == parent

    def test__load_yaml__plain__returns_correct_dict(self, change_dir):
        expected = {"ToBe": "ThatIs", "OrNot": "TheQuestion"}
        loaded = load_yaml(Path("../yaml/simple.yaml"))
        assert loaded == expected

    def test__load_yaml__with_simple_include__returns_correct_dict(self, change_dir):
        expected = {"Is": {"ToBe": "ThatIs", "OrNot": "TheQuestion"}}
        loaded = load_yaml(Path("../yaml/simple_include.yaml"))
        assert loaded == expected

    def test__load_yaml__with_nested_include__returns_correct_dict(self, change_dir):
        expected = {
            "ToBe": {"ThatIs": {"Or": "maybe"}, "TheQuestion": {"not": "?"}},
            "OrNot": {"not": "?"},
        }
        loaded = load_yaml(Path("../yaml/a.yaml"))
        assert loaded == expected

    def test__load_yaml__encoded_plain__returns_correct_dict(self, change_dir):
        expected = {"Text": "an encoded string with Umlaute: äöü"}
        loaded = load_yaml((Path("../yaml/utf8encoded.yaml")), encoding="utf8")
        assert loaded == expected

    def test__load_yaml__encoded_include__returns_correct_dict(self, change_dir):
        expected = {"Parent": {"Text": "an encoded string with Umlaute: äöü"}, "Here": "Mehr Umlaute: ßüäö"}
        loaded = load_yaml((Path("../yaml/utf8include.yaml")), encoding="utf8")
        assert loaded == expected

    def test__load_yaml__multiple_calls_and_encodings__updates_encoding_between_calls(self, change_dir):
        expected = {"Parent": {"Text": "an encoded string with Umlaute: äöü"}, "Here": "Mehr Umlaute: ßüäö"}
        loaded_first = load_yaml((Path("../yaml/utf8include.yaml")), encoding="utf8")
        assert loaded_first == expected
        loaded_second = load_yaml((Path("../yaml/wininclude.yaml")), encoding="cp1252")
        assert loaded_second == expected

    def test__load_yaml__custom_path_resolver(self, change_dir):
        class CustomPathResolver(PathResolver):
            def __init__(self):
                super().__init__()
                self.last_file_pattern = ""

            def resolve_file_pattern(self, root_path: str, file_pattern: str):
                self.last_file_pattern = file_pattern
                return super().resolve_file_pattern(root_path, file_pattern)

        path_resolver = CustomPathResolver()
        load_yaml(Path("../yaml/simple_include.yaml"), path_resolver)
        assert path_resolver.last_file_pattern.endswith("simple.yaml")

    @pytest.mark.parametrize("file_name", ["my/non_yaml/file.csv", "my/non_yaml/file", "file.", "file.xml"])
    def test__validate_yaml_file_suffix__invalid__raises(self, file_name):
        with pytest.raises(Exception):
            validate_yaml_file_suffix(Path(file_name))

    @pytest.mark.parametrize("file_name", ["my/file.yml", "file.Yml", "my/file.yaml", "file.YAML", "file.yAmL"])
    def test__validate_yaml_file_suffix__valid__pass(self, file_name):
        validate_yaml_file_suffix(Path(file_name))
