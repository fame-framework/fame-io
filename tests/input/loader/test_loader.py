# SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
import io
import os

import pytest
from mockito import when
from yaml import CollectionNode, ScalarNode, SequenceNode, MappingNode

from fameio.input.loader import YamlLoaderError, FameYamlLoader
from tests.utils import assert_exception_contains
from tests.mock_utils import un_stub


@pytest.fixture
def loader() -> FameYamlLoader:
    """Returns new FameYamlLoader with a dummy Stream having no file root"""
    stream = io.StringIO("Sample Stream")
    stream.name = None
    return FameYamlLoader(stream)


class TestFameYamlLoader:
    def test__extract_inclusion_command__unsupported_node__raises(self, loader):
        with pytest.raises(YamlLoaderError) as e_info:
            loader.digest_include(CollectionNode("tag", "value"))
        assert_exception_contains(FameYamlLoader._ERR_NODE_TYPE, e_info)

    def test__extract_inclusion_command__scalar_node__returns_root_path(self, loader):
        result, _, _ = loader.digest_include(ScalarNode("tag", "value"))
        assert result == os.curdir

    def test__extract_inclusion_command__scalar_node__returns_file_pattern_from_value(self, loader):
        _, result, _ = loader.digest_include(ScalarNode("tag", "myFilePattern"))
        assert result == "myFilePattern"

    def test__extract_inclusion_command__scalar_node__returns_empty_node_pattern(self, loader):
        _, _, result = loader.digest_include(ScalarNode("tag", "value"))
        assert result == ""

    def test__extract_inclusion_command__sequence_node__empty_list__raises(self, loader):
        with pytest.raises(YamlLoaderError) as e_info:
            loader.digest_include(SequenceNode("tag", []))
        assert_exception_contains(FameYamlLoader._ERR_ARGUMENT_COUNT, e_info)

    def test__extract_inclusion_command__sequence_node__too_many_entries__raises(self, loader, un_stub):
        node = self._stub_sequence_constructor(loader, args=[1, 2, 3])
        with pytest.raises(YamlLoaderError) as e_info:
            loader.digest_include(node)
        assert_exception_contains(FameYamlLoader._ERR_ARGUMENT_COUNT, e_info)

    @staticmethod
    def _stub_sequence_constructor(loader: FameYamlLoader, args: list) -> SequenceNode:
        """Stubs YamlLoader.construct_sequence to return given `args` when provided with returned SequenceNode"""
        node = SequenceNode(tag="tag", value="Dummy")
        when(loader).construct_sequence(node).thenReturn(args)
        return node

    def test__extract_inclusion_command__sequence_node__returns_root_path(self, loader, un_stub):
        node = self._stub_sequence_constructor(loader, args=["value"])
        result, _, _ = loader.digest_include(node)
        assert result == os.curdir

    def test__extract_inclusion_command__sequence_node__one_entry__returns_file_pattern(self, loader, un_stub):
        node = self._stub_sequence_constructor(loader, args=["myFilePattern"])
        _, result, _ = loader.digest_include(node)
        assert result == "myFilePattern"

    def test__extract_inclusion_command__sequence_node__one_entry__returns_empty_node_pattern(self, loader, un_stub):
        node = self._stub_sequence_constructor(loader, args=["value"])
        _, _, result = loader.digest_include(node)
        assert result == ""

    def test__extract_inclusion_command__sequence_node__two_entries__returns_both_patterns(self, loader, un_stub):
        node = self._stub_sequence_constructor(loader, args=["myFilePattern", "myNodePattern"])
        _, file_pattern, node_pattern = loader.digest_include(node)
        assert file_pattern == "myFilePattern"
        assert node_pattern == "myNodePattern"

    def test__extract_inclusion_command__mapping_node__empty_dict__raises(self, loader, un_stub):
        with pytest.raises(YamlLoaderError) as e_info:
            loader.digest_include(MappingNode("tag", {}))
        assert_exception_contains(FameYamlLoader._ERR_FILE_KEY_MISSING, e_info)

    @staticmethod
    def _stub_mapping_constructor(loader: FameYamlLoader, kwargs: dict) -> MappingNode:
        """Stubs YamlLoader.construct_mapping to return given `kwargs` when provided with returned MappingNode"""
        node = MappingNode(tag="tag", value="Dummy")
        when(loader).construct_mapping(node).thenReturn(kwargs)
        return node

    def test__extract_inclusion_command__mapping_node__missing_keyword_file__raises(self, loader, un_stub):
        with pytest.raises(YamlLoaderError) as e_info:
            loader.digest_include(self._stub_mapping_constructor(loader, {"A": "B", "node": 2}))
        assert_exception_contains(FameYamlLoader._ERR_FILE_KEY_MISSING, e_info)

    def test__extract_inclusion_command__mapping_node__file_provided__returns_root_path(self, loader, un_stub):
        node = self._stub_mapping_constructor(loader, {"file": "value"})
        result, _, _ = loader.digest_include(node)
        assert result == os.curdir

    def test__extract_inclusion_command__mapping_node__file_provided__returns_file_pattern(self, loader, un_stub):
        node = self._stub_mapping_constructor(loader, {"file": "myFilePattern"})
        _, result, _ = loader.digest_include(node)
        assert result == "myFilePattern"

    def test__extract_inclusion_command__mapping_node__both_provided__returns_both_patterns(self, loader, un_stub):
        node = self._stub_mapping_constructor(loader, {"file": "myFilePattern", "node": "myNodePattern"})
        _, file_pattern, node_pattern = loader.digest_include(node)
        assert file_pattern == "myFilePattern"
        assert node_pattern == "myNodePattern"

    @pytest.mark.parametrize(
        "kwargs", [{"file": "1", "node": "2"}, {"FILE": "1", "NODE": "2"}, {"fILe": "1", "NodE": "2"}]
    )
    def test__extract_inclusion_command__mapping_node__mixed_case__returns_both(self, loader, un_stub, kwargs):
        node = self._stub_mapping_constructor(loader, kwargs)
        assert loader.digest_include(node) == (os.curdir, "1", "2")
