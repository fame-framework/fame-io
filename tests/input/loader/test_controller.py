# SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
import pytest
from mockito import mock, when

from fameio.input.resolver import PathResolver
from fameio.input.loader import LoaderController, YamlLoaderError
from tests.utils import assert_exception_contains
from tests.mock_utils import un_stub
from tests.log_utils import fame_log


class TestLoaderController:
    _SAMPLE_DICT: dict = {"Flat": 1, "Nested": {"L1": {"L2": {"L3": "L4"}}}}
    _ROOT = "."
    _PATTERN = "myFilePattern"

    def test_resolve_imported_path__empty_match__returns_empty_list(self, un_stub):
        controller = self._setup_controller([])
        result = controller._resolve_imported_path(self._ROOT, self._PATTERN)
        assert result == []

    def _setup_controller(self, resolved_files: list) -> LoaderController:
        """Returns a new LoaderController with a mocked Resolver that "finds" the given `resolved_files`"""
        resolver = mock(PathResolver)
        when(resolver).resolve_file_pattern(self._ROOT, self._PATTERN).thenReturn(resolved_files)
        return LoaderController(resolver)

    def test_resolve_imported_path__empty_match__logs_warning(self, un_stub, fame_log):
        controller = self._setup_controller([])
        controller._resolve_imported_path(self._ROOT, self._PATTERN)
        fame_log.assert_logged(LoaderController._WARN_NOTHING_TO_INCLUDE)

    def test_resolve_imported_path__matches_files__returns_file_list(self, un_stub):
        controller = self._setup_controller(["a", "b", "c"])
        result = controller._resolve_imported_path(self._ROOT, self._PATTERN)
        assert result == ["a", "b", "c"]

    def test_resolve_imported_path__all_files_ignored__returns_empty_list(self, un_stub):
        controller = self._setup_controller([LoaderController.DISABLING_YAML_FILE_PREFIX + "ignoredFile"])
        result = controller._resolve_imported_path(self._ROOT, self._PATTERN)
        assert result == []

    def test_resolve_imported_path__some_files_ignored__returns_not_ignored_files(self, un_stub):
        files = [
            LoaderController.DISABLING_YAML_FILE_PREFIX + "ignoredFileA",
            "X",
            "Y",
            LoaderController.DISABLING_YAML_FILE_PREFIX + "ignoredFileB",
            "Z",
        ]
        controller = self._setup_controller(files)
        result = controller._resolve_imported_path(self._ROOT, self._PATTERN)
        assert result == ["X", "Y", "Z"]

    def test__extract_node__no_nodes__returns_data(self):
        result = LoaderController._extract_node(file_name="MyFile", data=self._SAMPLE_DICT, node_address=[])
        assert result == self._SAMPLE_DICT

    def test__extract_node__flat_node_selected__returns_value(self):
        result = LoaderController._extract_node(file_name="MyFile", data=self._SAMPLE_DICT, node_address=["Flat"])
        assert result == 1

    @pytest.mark.parametrize(
        "nodes, expected",
        [
            (["Nested"], {"L1": {"L2": {"L3": "L4"}}}),
            (["Nested", "L1"], {"L2": {"L3": "L4"}}),
            (["Nested", "L1", "L2"], {"L3": "L4"}),
        ],
    )
    def test__extract_node__inner_node_selected__returns_next_inner_dict(self, nodes, expected):
        result = LoaderController._extract_node(file_name="MyFile", data=self._SAMPLE_DICT, node_address=nodes)
        assert result == expected

    @pytest.mark.parametrize(
        "nodes", [["MissingTopLevelNode"], ["Nested", "MissingMiddleNode"], ["Nested", "L1", "L2", "MissingInnerNode"]]
    )
    def test__extract_node__node_missing__raises(self, nodes):
        with pytest.raises(YamlLoaderError) as e_info:
            LoaderController._extract_node(file_name="MyFile", data=self._SAMPLE_DICT, node_address=nodes)
        assert_exception_contains(LoaderController._ERR_NODE_MISSING, e_info)

    def test__join_data__previous_data_is_none__returns_new_data(self):
        # noinspection PyTypeChecker
        result = LoaderController._join_data([1, 2], None)
        assert result == [1, 2]

    def test__join_data__previous_data_is_empty__returns_new_data(self):
        result = LoaderController._join_data([1, 2], [])
        assert result == [1, 2]

    def test__join_data__new_data_is_empty__return_previous_data(self):
        result = LoaderController._join_data([], [1, 2])
        assert result == [1, 2]

    def test__join_data__new_and_previous_data_not_empty__extends_previous(self):
        result = LoaderController._join_data([4, 5], [1, 2, 3])
        assert result == [1, 2, 3, 4, 5]

    @pytest.mark.parametrize("new, previous", [([1], {1: 2}), ("Not A list", [1])])
    def test__join_data__not_all_lists__raises(self, new, previous):
        with pytest.raises(YamlLoaderError) as e_info:
            # noinspection PyTypeChecker
            LoaderController._join_data(new, previous)
        assert_exception_contains(LoaderController._ERR_NOT_LIST, e_info)
