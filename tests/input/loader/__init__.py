# SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: CC0-1.0

import sys

sys.path.append("../../../src")
