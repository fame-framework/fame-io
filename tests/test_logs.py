# SPDX-FileCopyrightText: 2025 German Aerospace Center <fame@dlr.de>
#
# SPDX-License-Identifier: Apache-2.0
import logging as pylog
from pathlib import Path

import pytest
from mockito import expect, verifyNoUnwantedInteractions

from fameio.logs import (
    log_error_and_raise,
    fameio_logger,
    _INFO_UPDATING_LOG_LEVEL,  # noqa
    _FORMAT_DETAILLED,  # noqa
    _FORMAT_NORMAL,  # noqa
    _WARN_NOT_INITIALIZED,  # noqa
    _loggers,  # noqa
    log,
    log_critical_and_raise,
    LOGGER_NAME,
    DEFAULT_LOG_LEVEL,
    LogLevel,
    log_error,
    log_critical,
)
from tests.log_utils import LogTester
from tests.utils import assert_exception_contains


@pytest.fixture
def clear_logs() -> None:
    _loggers.clear()
    yield
    _loggers.clear()


class TestLogs:
    def test__logger__uninitialised__logs_warning(self, caplog, clear_logs):
        caplog.set_level(pylog.WARNING)
        log()
        LogTester.assert_expected_message_in_actual(_WARN_NOT_INITIALIZED, caplog.text)

    def test__logger__uninitialised__returns_default_logger(self, clear_logs):
        assert log() == pylog.getLogger(LOGGER_NAME)
        assert log().level == DEFAULT_LOG_LEVEL.value

    @pytest.mark.parametrize("ex", [ValueError("A"), IOError("ABC"), Exception("Help!")])
    def test__log_error__logs_given_exception_as_str_with_level_error(self, ex: Exception, caplog, clear_logs):
        fameio_logger("error")
        log().propagate = True
        log_error(ex)
        LogTester.assert_expected_message_in_actual(str(ex), caplog.text)

    @pytest.mark.parametrize("ex", [ValueError("A"), IOError("ABC"), Exception("Help!")])
    def test__log_error__returns_given_exception(self, ex: Exception):
        result = log_error(ex)
        assert result == ex

    @pytest.mark.parametrize("ex", [ValueError("A"), IOError("ABC"), Exception("Help!")])
    def test__log_error_and_raise__logs_given_exception_as_str(self, ex: Exception, caplog, clear_logs):
        fameio_logger("error")
        log().propagate = True
        with pytest.raises(Exception) as e_info:
            log_error_and_raise(ex)
        LogTester.assert_expected_message_in_actual(str(ex), caplog.text)
        assert_exception_contains(str(ex), e_info)

    def test__log_critical_and_raise__logs_message(self, caplog, clear_logs):
        fameio_logger("critical")
        log().propagate = True
        with pytest.raises(Exception) as e_info:
            log_critical_and_raise(Exception("MyMessage"))
        # noinspection PyUnreachable
        LogTester.assert_expected_message_in_actual("MyMessage", caplog.text)
        assert_exception_contains("MyMessage", e_info)

    def test__log_critical__logs_given_exception_as_str_with_level_critical(self, caplog, clear_logs):
        fameio_logger("critical")
        log().propagate = True
        log_critical(Exception("MyMessage"))
        LogTester.assert_expected_message_in_actual("MyMessage", caplog.text)

    @pytest.mark.parametrize("ex", [ValueError("A"), IOError("ABC"), Exception("Help!")])
    def test__log_critical__returns_given_exception(self, ex):
        result = log_critical(ex)
        assert result == ex

    def test_fameio_logger__already_handled__logs_warning(self, caplog, clear_logs):
        caplog.set_level(pylog.INFO)
        fameio_logger("info")
        fameio_logger("error")
        LogTester.assert_expected_message_in_actual(_INFO_UPDATING_LOG_LEVEL, caplog.text)

    def test_fameio_logger__called_twice__level_most_recent(self, clear_logs):
        fameio_logger("info")
        assert log().level == pylog.INFO
        fameio_logger("error")
        assert log().level == pylog.ERROR
        fameio_logger("debug")
        assert log().level == pylog.DEBUG

    def test_fameio_logger__called_twice__removed_previous_handlers(self, clear_logs):
        fameio_logger(log_level_name="info", file_name=Path("deleteMe.ignore"))
        fameio_logger("error")
        assert len(log().handlers) == 1
        assert isinstance(log().handlers[0], pylog.StreamHandler)

    def test_fameio_logger__level_debug__detailed_format(self, clear_logs):
        with expect(pylog, times=1).Formatter(_FORMAT_DETAILLED, any):
            fameio_logger("debug")
        verifyNoUnwantedInteractions()

    @pytest.mark.parametrize("level", ["info", "warn", "error", "critical"])
    def test_fameio_logger__other_level__short_format(self, level, clear_logs):
        with expect(pylog, times=1).Formatter(_FORMAT_NORMAL, any):
            fameio_logger(level)
        verifyNoUnwantedInteractions()

    @pytest.mark.parametrize("level", ["info", "warn", "warning", "error", "critical", "debug"])
    def test_fameio_logger__log_level__is_set(self, level, clear_logs):
        fameio_logger(level)
        assert log().getEffectiveLevel() == LogLevel[level.upper()].value

    def test_fameio_logger__file_name_exists__adds_file_handler(self, clear_logs):
        fameio_logger(log_level_name="info", file_name=Path("deleteMe.ignore"))
        assert any([isinstance(handler, pylog.FileHandler) for handler in log().handlers])
