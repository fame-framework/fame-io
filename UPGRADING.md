<!-- SPDX-FileCopyrightText: 2024 German Aerospace Center <fame@dlr.de>

SPDX-License-Identifier: Apache-2.0 -->

# Upgrading

## 3.0.0

### Updated protobuf definitions

This version is based on new definitions of fameprotobuf version 2.0.2.
Protobuf files created with this version of fameio are compatible with applications based on FAME-Core v2.0, but
incompatible with those using an earlier version of FAME-Core.
Also, protobuf files created with earlier versions of FAME-Core cannot be read with fameio as data representation has
changed internally.
An error will be raised if one tries to access old files with this version of fameio.
Thus, use fameio in version 2.3 to access files written with previous versions of FAME-Core or fameio.

### Scenario - removed section GeneralProperties.Output

This version no longer uses the subsection `Output` in section `GeneralProperties` of scenarios.
Any content in that subsection will be ignored.
Parameters previously given there are now configured using FAME-Core when starting up a FAME simulation.

### Schema - section JavaPackages becomes mandatory

In this version, specifying the section `JavaPackages` in schema becomes mandatory.
An error will be raised if this section in missing.

### Command line - changed time-merging

In this version, the command-line argument `--time-merging` for `convert` is no longer parsed by a subparser but a
threefold integer parser.
Thus, parametrising becomes easier and changes from, e.g.,`merge-times -fp 0 -sb 1 -sa 2` to `--merge-times 0 1 2`.
The data type of `Options.TIME_MERGING` has changed from a dictionary to a list.
Update you calls to fameio's command line and / or calls to `fameio.source.cli.convert_results.handle_args()`
accordingly.

### PathResolver renaming

In this version, file `path_resolver.py` was renamed to `resolver.py`.
Furthermore, the method `PathResolver.resolve_yaml_imported_file_pattern` was renamed to
`PathResolver.resolve_file_pattern` as this more accurately describes it.
If you call PathResolver directly, rename your imports and method calls accordingly.

### Restricted Attribute names `value`, `values`, and `metadata`

If your FAME model has an Agent with an Attribute using the keywords `value`, `values` or `metadata`, you will need to
rename it.
The reason for this is that these are restricted keywords necessary for implementing the metadata features of FAME
models.

### Unprotected KEY_WORDS in schema and scenario

All keywords in the `schema` and `scenario` packages have been renamed to lose their underscore at the beginning.
This makes them unprotected, and using these keywords in any script will no longer result in a linter warning.
However, if you use any of the keywords in your scripts directly, e.g. `Agent._KEY_TYPE`, `Contract._KEY_SENDER`,
or `Scenario._KEY_AGENTS`, you will need to remove the first underscore in their name.
This affects keywords in classes:

* `schema.`
    * `AgentType`, `AttributeSpecs`, `JavaPackages`, `Schema`
* `scenario.`
    * `Agent`, `Contract`, `GeneralProperties`, `Scenario`, `StringSet`

### New package structure

Package `source` was removed.
Its content was moved to new high-level packages:
* `source.cli` --> `cli`
* `source.results` --> `output`

The following files and packages were moved below the new high-level package `input`:
* Files: writer.py, validator.py, resolver.py, metadata.py
* All former packages `source.loader`, `source.schema`, `source.scenario`

Files logs.py, series.py, time.py, and tools.py were moved to top-level package `fameio`.

This simplifies and homogenises the package structure.
If you call any methods from the above-mentioned packages, please adjust the import statements accordingly.
In case you only use or import `make_config` or `convert_results` methods, no action is required.


### Renamed Exceptions to Errors

In order to sound more Pythonic, Exception classes were renamed to Errors:

* `fameio.time`: ConversionException --> Conversion**Error**
* `fameio.series`: TimeSeriesException --> TimeSeries**Error**
* `fameio.output.reader`: ProtobufReaderException --> ProtobufReader**Error**
* `fameio.output.input_dao`: InputConversionException --> InputConversion**Error**
* `fameio.input.writer`: ProtoWriterException --> ProtoWriter**Error**
* `fameio.input.validator`: ValidationException --> Validation**Error**
* `fameio.input.schema.exception`: SchemaException --> **fameio.**Schema**Error**
* `fameio.input.scenario.exception`: ScenarioException --> **fameio.**Scenario**Error**
* `fameio.input.loader.exception`: YamlLoaderError --> **fameio.**YamlLoaderError

If you are using any of these now deprecated Exceptions, refactor their names to match the new Error.
